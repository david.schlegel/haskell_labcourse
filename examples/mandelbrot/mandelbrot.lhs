compile with

    ghc -Odph -rtsopts -threaded -fno-liberate-case -funfolding-use-threshold1000 -funfolding-keeness-factor1000 -fllvm -optlo-O3 -eventlog mandelbrot.lhs

run with (N is the number of threa2015-08-12)

    ./mandelbrot +RTS -s -N2 -l

visualize

    ../../vis.py -x xmat.txt -y ymat.txt -z zmat.txt -t image

and show what the threads are doing

    threadscope mandelbrot.eventlog

\begin{code}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE BangPatterns #-}


import Data.Array.Repa as R
import Data.Array.Repa.Algorithms.Complex
import Data.Array.Repa.Repr.HintInterleave
import Data.List (findIndex)
import Data.Packed.Repa
import Numeric.Container (saveMatrix)


-- checks if sequence is infinite (and then returns the number of iterations it needed to determine that)
isGeneralMandel :: (Complex -> Complex -> Complex) -> Int -> Double -> Complex -> Maybe Int
isGeneralMandel f iteration_depth bound c = findIndex ((>bound) . mag) (take iteration_depth (iterate (f c) 0))

-- checks if given number is in mandelbrot set
isMandel :: Int -> Double -> Complex -> Maybe Int
isMandel =  isGeneralMandel (\c z -> z*z + c)

-- grid: values in complex plane
grid :: (Int, Int) -> DIM2
grid (x, y) = (Z :. x) :. y 

-- calculates the mesh of points in complex plane
calcView :: (Complex, Complex) -> (Int, Int) -> DIM2 -> Complex
calcView ((left, bottom), (right, top)) (max_x, max_y) (Z :. x :. y) = ((right - left) * (fromIntegral x)/(fromIntegral max_x - 1) + left, (top - bottom) * (fromIntegral y)/(fromIntegral max_y - 1) + bottom)

view = ((-1.5, -1.5),(1.5,1.5))

main :: IO ()
main = do 
    let size = (1024,1024)
    !coord <- computeP $ fromFunction (grid size) ( calcView view size) :: IO (Array U DIM2 Complex)

    let z = R.map ( maybe 0 fromIntegral . isMandel 35 3) coord :: Array D DIM2 Double
    !zmat  <- computeP $  hintInterleave z :: IO (Array U DIM2 Double)
    print "finished"
    -- write out matrices to visualize with python
    let xmat = copyS  $ R.map fst coord 
    let ymat = copyS  $ R.map snd coord 
    saveMatrix "xmat.txt" "%f" (repaToMatrix xmat)
    saveMatrix "ymat.txt" "%f" (repaToMatrix ymat)
    let zmats = copyS zmat
    saveMatrix "zmat.txt" "%f" (repaToMatrix zmats)
    -- ../vis.py -x xmat.txt -y ymat.txt -z zmat.txt -t image
\end{code}
