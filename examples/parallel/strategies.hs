-- ghc -threaded -rtsopts strategies.hs
import Control.Parallel.Strategies
import Control.Monad.IO.Class


fibs :: (Show a, Integral a) => a -> a
fibs n
     | n==0 || n==1 = 1
     | otherwise = fibs (n-1) + fibs (n-2)



evalm = runEval $ do
    fx <- rpar (fibs 35)
    fy <- rpar (fibs 35)
    return (fx, fy)

evals = runEval $ do
    fx <- rpar (fibs 35)
    fy <- rseq (fibs 35)
    return (fx, fy)

evalrss = runEval $ do
    fx <- rpar (fibs 35)
    fy <- rseq (fibs 35)
    rseq fx
    return (fx, fy)


evalstrat :: Strategy (a,b)
evalstrat (a,b) = do
    fx <- rpar a
    fy <- rpar b 
    return (fx, fy)


parMap :: (a -> b) -> [a] -> [b]
parMap f xs = withStrategy (parList rseq) $ map f xs



main :: IO ()
main = do
    -- print $ runEval (evalstrat (fibs 35, fibs 35))
    -- print $ withStrategy (evalstrat) (fibs 35, fibs 35)
    -- print $ withStrategy (rpar ) (fibs 35, fibs 35)
    print $ Main.parMap fibs [35,35,35,35] 

    --print evalm
