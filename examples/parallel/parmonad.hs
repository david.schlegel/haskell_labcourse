-- ghc -threaded -rtsopts parmnonad.hs
import Control.Monad.Par

fibs :: (Show a, Integral a) => a -> a
fibs n
     | n==0 || n==1 = 1
     | otherwise = fibs (n-1) + fibs (n-2)

main = do
    print $ runPar $ do
        i <- new
        j <- new
        fork (put i (fibs 35::Int))
        fork (put j (fibs 35::Int))
        fx <- get i
        fy <- get j
        return (fx, fy)
