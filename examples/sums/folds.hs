-- ghc  -O2 -prof -auto-all -caf-all -rtsopts folds.hs && ./folds +RTS -p  -RTS
import Data.List
sumlTr :: Num a => a -> [a] -> a
sumlTr x [] = x
sumlTr x (y:ys) = sumlTr (x+y) ys

sumlD :: [Int] -> Int
sumlD [] = 0
sumlD (y:ys) = y + sumlD ys

mapm :: (a -> b) -> [a] -> [b]
mapm f xs = foldr (\x acc -> f x : acc) [] xs

mapm2 :: (a -> b) -> [a] -> [b]
mapm2 f xs = foldr (\x acc -> [f x] ++ acc) [] xs

main :: IO ()
main = do
    let a = sumlTr 0 [1..100000]
    print a
    let b = sumlD [1..100000]
    print b
    let c = foldl (+) 0  [1..100000]
    print c
    let d = foldl' (+) 0  [1..100000]
    print d
    let e = foldr (+) 0  [1..100000]
    print e
    let f = mapm (+3) [1..100000]
    print f
    let g = mapm2 (+3) [1..100000]
    print g
