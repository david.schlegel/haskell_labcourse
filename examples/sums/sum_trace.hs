import Debug.HTrace

sumr :: Int -> Int
sumr n | htrace ("sumr " ++ show n ) False = undefined
sumr n
    | n == 0    = 0
    | otherwise =  htrace "+" (n +  sumr (htrace "-" (n-1)) )

sumtr :: Int -> Int
sumtr n = addSum 0 n 
addSum m n | htrace ("addSum " ++ show n ) False = undefined
-- Vorsicht: macht addSum in m und n strict
addSum m n
      | n == 0      = m
      | otherwise   = addSum (htrace "+" m+n) (htrace "-" n-1)

main :: IO ()
main = do
    print "tail recursion"
    let a = sumtr 3
    print a
    print "no tail recursion"
    let b = sumr 3
    print b
