{-# OPTIONS_GHC -O3 #-}
{-# LANGUAGE FlexibleContexts #-}

import Data.Array.Repa as Repa hiding ((++))
import Codec.Picture.Repa (readImageRGB, imgData)
import Data.Array.Repa.Eval (Load(..))
import Control.Monad

type Img r = Array r DIM2 Double

loadImage :: IO (Img U)
loadImage = do
     result <- readImageRGB "grumpy.png"
     let imageRGB = case result of
                      Left err  -> error err
                      Right img -> img
     return . sumS . Repa.map fromIntegral . imgData $ imageRGB

compS :: Load r DIM2 Double => Img r -> Img U
compS = computeS

compP :: (Monad m, Load r DIM2 Double) => Img r -> m (Img U)
compP = computeP

filter1d :: Source r Double => (Double -> Double -> Double) -> Img r -> Img D
filter1d op img = traverse img resize modify
    where resize (Z:.i:.j) = Z:.i:.(j `div` 2)
          modify get (Z:.i:.j) = get (Z:.i:.(2*j)) `op` get (Z:.i:.(2*j+1))

step1d :: Source r Double => Img r -> Img D
step1d img = append (filter1d lowpass img) (filter1d highpass img)
    where lowpass x y = (x + y) / 2
          highpass x y = (x - y) / 2

waveletStep :: (Monad m, Source r Double) => Img r -> m (Img U)
waveletStep = compP . transpose . step1d . transpose . step1d

insertTopLeft :: (Source r1 Double, Source r2 Double) => Img r1 -> Img r2 -> Img D
insertTopLeft img topleft = traverse img id modify
    where sh = extent topleft
          modify get idx
              | inShape sh idx = topleft ! idx
              | otherwise      = get idx

extractTopLeft :: Source r Double => Img r -> Img D
extractTopLeft img = extract (Z:.0:.0) (Z:.(sx `div` 2):.(sy `div` 2)) img
    where Z:.sx:.sy = extent img

waveletTransform :: (Monad m, Load r DIM2 Double) => Int -> Img r -> m (Img U)
waveletTransform 0 img = compP img
waveletTransform n img = do
  img' <- waveletStep img
  topleft <- waveletTransform (n-1) $ extractTopLeft img'
  compP $ insertTopLeft img' topleft

inverseFilter1d :: Source r Double => (Double -> Double -> Double) -> Img r -> Img D
inverseFilter1d op img = Repa.zipWith op left right
    where Z:.sx:.sy = extent img
          half      = Z:.sx:.(sy `div` 2)
          left      = extract (Z:.0:.0) half img
          right     = extract (Z:.0:.(sy `div` 2)) half img

inverseStep1d :: Source r Double => Img r -> Img D
inverseStep1d img = interleave2 (inverseFilter1d (+) img) (inverseFilter1d (-) img)

inverseWaveletStep :: (Monad m, Source r Double) => Img r -> m (Img U)
inverseWaveletStep img = do
  aux <- compP . inverseStep1d . transpose $ img
  compP . inverseStep1d . transpose $ aux

inverseWaveletTransform :: (Monad m, Load r DIM2 Double) => Int -> Img r -> m (Img U)
inverseWaveletTransform 0 img = compP img
inverseWaveletTransform n img = do
  topleft <- inverseWaveletTransform (n-1) $ extractTopLeft img
  let img' = insertTopLeft img topleft
  inverseWaveletStep img'

truncateCoeffs :: (Monad m, Source r Double) => Double -> Img r -> m (Img D)
truncateCoeffs threshold img = do
  sumabs <- sumAllP . Repa.map abs $ img
  let mean = sumabs / (fromIntegral . size . extent $ img)
  return $ Repa.map (f mean) img
      where f mean x | abs x < threshold*mean = 0
                     | otherwise              = x

norm2 :: Source r Double => Img r -> Double
norm2 img = sumAllS . Repa.map (^(2::Int)) $ img

relError :: (Source r1 Double, Source r2 Double) => Img r1 -> Img r2 -> Double
relError a b = sqrt (norm2 (a -^ b) / norm2 a)

countZeros :: Source r Double => Img r -> Int
countZeros = round . foldAllS (\acc x -> if x == 0 then acc+1 else acc) 0

main :: IO ()
main = do
  let n = 4
  image <- loadImage
  putStrLn $ "Number of elements:       " ++ show (size . extent $ image)

  transformed <- waveletTransform n . delay $ image
  putStrLn $ "Zeros in non-truncated:   " ++ show (countZeros transformed)

  truncated <- liftM compS $ truncateCoeffs 0.1 transformed
  putStrLn $ "Zeros in truncated:       " ++ show (countZeros truncated)
  putStrLn $ "Relative error in coeffs: " ++ show (relError transformed truncated)

  compressed <- inverseWaveletTransform n . delay $ truncated
  putStrLn $ "Relative error in image:  " ++ show (relError image compressed)
