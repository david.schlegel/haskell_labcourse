-- ghc  -O2 -prof -fprof-auto -rtsopts factorial.hs && ./factorial +RTS -pi -hc -sstderr -RTS
{-# LANGUAGE BangPatterns #-}
facE :: (Show a, Integral a) => a -> a
facE i
      | i<0 = error "Negative Faculty not defined"
      | i==0 = 1
      | otherwise = i* facE (i-1)

facNE :: (Show a, Integral a) => a -> a
facNE 0 = 1
facNE i | i > 0 = i* facNE (i-1)

facR :: (Show a,Integral a) => a -> a
facR x = fac' x 1 where
    fac' 1 y = y
    fac' f y = fac' (f-1) (f*y)

facRf :: (Show a, Integral a) => a -> a
facRf x = fac' x 1 where
    fac' 1 y = y
    fac' f y = fac'  (f-1) $! (f*y)

main :: IO ()
main = do
    let g = facNE 10000
    print g
    let f = facR 10000
    print f
    let h = facRf 10000
    print h
