import Debug.HTrace
facE :: (Show a, Integral a) => a -> a
facE x | htrace ("facE " ++ show x ) False = undefined
facE i
      | i<0 = error "Negative Faculty not defined"
      | i==0 = 1
      | otherwise = i* facE (i-1)

facNE :: (Show a, Integral a) => a -> a
facNE x | htrace ("facNE " ++ show x ) False = undefined
facNE 0 = 1
facNE i | i > 0 = htrace "*" (i* facNE (htrace "-" (i-1)))

facR :: (Show a,Integral a) => a -> a
facR x | htrace ("facR " ++ show x ) False = undefined
facR x = fac' x 1 where
    fac' 1 y = y
    fac' f y = fac' (htrace "-" (f-1)) (htrace "*" (f*y)) 

facRf :: (Show a, Integral a) => a -> a
facRf x | htrace ("facRf " ++ show x ) False = undefined
facRf x = fac' x 1 where
    fac' 1 y = y
    fac' f y = fac' (htrace "-" (f-1)) $! (htrace "*" (f*y)) 


main = do
    let g = facNE 4
    print g
    let f = facR 4
    print f
    let h = facRf 4
    print h
