
compile with
    
    ghc -Odph -rtsopts -threaded -fno-liberate-case -funfolding-use-threshold1000 -funfolding-keeness-factor1000 -fllvm -optlo-O3 -eventlog newton.hs

run with (N is the number of threa2015-08-12)
    
    ./newton +RTS -s -N2 -l

visualize 

    ../../vis.py -x xmat.txt -y ymat.txt -z zmat.txt -t image

and show what the threads are doing

    threadscope newton.eventlog 

\begin{code}
{-# Language BangPatterns #-}

import Numeric.Container
import Data.Complex
import Data.Array.Repa as R
import Data.List (genericLength)
import Data.Packed.Repa

newton :: (Complex Double -> Complex Double) -> Complex Double-> [(Complex Double,Complex Double)]
newton f xn = (f xn,xn) : newton f (fn xn)
    where fn xn = let h = 1e-11 in xn - f xn/((f (xn+h) - f xn)/h)

f !x = x**3 - 1

nm :: Complex Double -> (Double, Double)
nm !g = ((\x -> (phase . last $ x, genericLength x) ) . snd . unzip . takeWhile (\(y,_) -> magnitude y >1e-13) . newton f) g

n = 1000 -- size of the domain
coord = linspace n (-0.9,0.9::Double) --coordinates in one dimension
-- get complex values in a grid
m = fromFunction (Z :. n :. n) (\(Z :. x :. y) -> coord @> x :+ coord @> y ) 


main = do  
    -- create matrix of the mesh (and newton iteration start values)
    let coordm = computeUnboxedS m  
   
    -- solution of newton iteration for every startvalue
    let nv = R.map (nm) coordm
    solm <- computeUnboxedP nv
    let phasem = R.map fst solm
    saveMatrix "zmatphase.txt" "%f" (repaToMatrix $ copyS $ phasem)
    -- number of iterations for every startvalue
    let numitm = R.map snd solm
    saveMatrix "zmat.txt" "%f" (repaToMatrix $ copyS $ numitm)
    let xmat = copyS $ R.map (realPart) coordm
    let ymat = copyS $ R.map (imagPart) coordm
    saveMatrix "xmat.txt" "%f" (repaToMatrix  xmat)
    saveMatrix "ymat.txt" "%f" (repaToMatrix  ymat)
\end{code}


