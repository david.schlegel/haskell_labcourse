{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
import qualified Data.ByteString.Lazy as BL
import Control.Applicative
import Data.Csv
import qualified Data.Vector as V
import GHC.Generics

import Graphics.Rendering.Chart
import Graphics.Rendering.Chart.Backend.Cairo
import Control.Lens
import Data.Default.Class
import Data.Colour
import Data.Colour.Names

import Data.List (minimumBy,transpose,sortBy)
import Data.List.Extras.Argmax (argmin)
import qualified Data.Map as M
import Data.Ord (comparing)

--------------------------------------------------
-- types 

data IrisSpec = Setosa | Versicolour | Virginica
                deriving (Eq,Ord,Show,Enum)

instance FromField IrisSpec where
    parseField s
        | s == "Iris-setosa"        = pure Setosa
        | s == "Iris-versicolor"    = pure Versicolour
        | s == "Iris-virginica"     = pure Virginica
        | otherwise                 = empty

colourOf :: (Floating a, Ord a) => IrisSpec -> Colour a
colourOf Setosa         = red
colourOf Versicolour    = greenyellow
colourOf Virginica      = blue

instance FromField Bool where
    parseField s
        | s == "True"   = pure True
        | s == "true"   = pure True
        | s == "1"      = pure True
        | s == "False"  = pure False
        | s == "false"  = pure False
        | s == "0"      = pure False
        | otherwise     = empty

data Iris = Iris {
    sepalLength :: Double,
    sepalWidth :: Double,
    petalLength :: Double,
    petalWidth :: Double,
    species :: IrisSpec,
    testSet :: Bool
    } deriving (Show,Generic)

instance FromRecord Iris

type IrisSet = V.Vector Iris

--------------------------------------------------
-- main

main :: IO ()
main = do
    csvData <- BL.readFile "iris.data"
    let dataSet = decode NoHeader csvData :: Either String IrisSet
    either (\x -> error $ "no parse :( " ++ x) handleData dataSet


handleData :: IrisSet -> IO ()
handleData v = do
    _ <- plotRawData v
    _ <- plotKMeans v 
    _ <- plotAnn v 
    putStrLn "done"

--------------------------------------------------
-- kMeans

type DPoint = [Double]

toPoint :: Iris -> DPoint
toPoint x = sepalLength x:sepalWidth x:petalLength x:petalWidth x:[]

dist :: DPoint -> DPoint -> Double
dist a b = sqrt . sum $ map (**2) $ zipWith (-) a b

centroid :: [DPoint] -> DPoint
centroid ps = map (\xs -> sum xs / l) $ transpose ps  
    where l = fromIntegral $ length ps


recluster :: [DPoint] -> [DPoint] -> [[DPoint]]
recluster cs ps = map (\c -> [p | p <- ps, c == nearestCent cs p]) cs 

nearestCent :: [DPoint] -> DPoint -> DPoint
nearestCent cs p = argmin (dist p) cs 

kMeans' :: [[DPoint]] -> [[DPoint]]
kMeans' cps = if converged
    then cps
    else kMeans' ncps 
        where 
            cs = map centroid cps
            ncps = recluster cs $ concat cps
            ncs = map centroid ncps
            converged = ncs == cs


kMeans :: Int -> [DPoint] -> [[DPoint]]
kMeans k ps = kMeans' $ recluster cs ps
    where
        cs = take k ps 

--------------------------------------------------
-- perceptron

dot :: DPoint -> DPoint -> Double
dot a b = sum $ zipWith (*) a b

neuronOutput :: DPoint -> Double -> DPoint -> Double
neuronOutput w b x = if w `dot` x + b > 0
    then 1
    else (-1)

ann :: [Iris] -> [(Iris, IrisSpec)]
ann is = [ (i,annClass $ toPoint i) | i <- learnSet] 
    where
        trainSet = filter (not . testSet) is'
        learnSet = filter (testSet) is'
        is' = filter ((Versicolour /=) . species) is
        annClass i = noToSpec $ neuronOutput tw tb i
        (tw,tb) = trainAnn iw ib trainSet 
        iw = replicate l 0
        ib = 0
        l = length $ toPoint $ head is

noToSpec :: Double -> IrisSpec
noToSpec x
    | x > 0     = Setosa
    | otherwise = Virginica

specToNo :: IrisSpec -> Double
specToNo Setosa     = 1
specToNo Virginica  = (-1)
specToNo _ = error "specToNo: no such spec"

alpha :: Double
alpha = 0.005

gamma :: Double
gamma = 0.01 

trainAnn :: [Double] -> Double -> [Iris] -> ([Double], Double)
trainAnn w b ts = if converged
    then (nw,nb)
    else trainAnn nw nb ts
        where
            converged = gamma >= 1/s * sum (map abs err)
            err = zipWith (-) desiredOutput actualOutput 
            s = fromIntegral $ length ts
            actualOutput = map (neuronOutput w b . toPoint) ts
            desiredOutput = map (specToNo . species) ts 
            nw = zipWith (+) w dw
            aerr = map (* alpha) err
            dw = map (sum . zipWith (*) aerr) (transpose $ map toPoint ts) 
            nb = b + sum aerr

--------------------------------------------------
-- plots

foOptions :: FileOptions
foOptions = (fo_size .~ (1280,1024) $ def)

plotAnn :: IrisSet -> IO (PickFn ())
plotAnn v = renderableToFile foOptions chart "ann.png"
    where
        chart = toRenderable layout
        pointsOfCluster s = plot_points_style .~ 
                    hollowPolygon 5 0.5 5 True (opaque (colourOf s))
                $ plot_points_values .~
                    [ (sepalWidth e,petalWidth e) | (e,c) <- annps, c == s]
                $ plot_points_title .~ show s
                $ def
                where 
                    annps = ann $ V.toList v
        layout  = layout_title .~ "ANN classified data"
                $ layout_x_axis . laxis_title .~ "sepalWidth"
                $ layout_y_axis . laxis_title .~ "petalWidth"
                $ layout_plots .~ 
                    (map (toPlot . pointsOfCluster) [Setosa ..]
                    ++ map (toPlot . pointsOfSpecies v) [Setosa ..])
                $ def

plotKMeans :: IrisSet -> IO (PickFn ())
plotKMeans v = renderableToFile foOptions chart "kmeans.png"
    where
        chart = toRenderable layout
        pointsOfCluster c = plot_points_style .~
                     hollowCircles 4 0.5 (opaque (colourOfCluster c))
                $ plot_points_values .~
                    [ (e!!1,e!!3) | e <- c3ps!!c]
                $ plot_points_title .~ show c
                $ def
                where 
                    c3ps = kMeans 3 ps
                    ps = map toPoint $ V.toList v 
        colourOfCluster 0 = red
        colourOfCluster 1 = greenyellow
        colourOfCluster 2 = blue
        colourOfCluster _ = error "colourOfCluster: invalid argument"
        layout  = layout_title .~ "kMeans clustered data"
                $ layout_x_axis . laxis_title .~ "sepalWidth"
                $ layout_y_axis . laxis_title .~ "petalWidth"
                $ layout_plots .~ 
                    (map (toPlot . pointsOfCluster) [0..2]
                    ++ map (toPlot . pointsOfSpecies v) [Setosa ..])
                $ def

plotRawData :: IrisSet -> IO (PickFn ())
plotRawData v = renderableToFile foOptions chart "raw_data.png"
    where
        chart = toRenderable layout
        layout  = layout_title .~ "Raw data"
                $ layout_x_axis . laxis_title .~ "sepalWidth"
                $ layout_y_axis . laxis_title .~ "petalWidth"
                $ layout_plots .~ 
                    (toPlot (testData v) : map (toPlot . pointsOfSpecies v) [Setosa ..])
                $ def

pointsOfSpecies :: V.Vector Iris -> IrisSpec -> PlotPoints Double Double
pointsOfSpecies v s = plot_points_style .~ filledCircles 2 (opaque (colourOf s))
    $ plot_points_values .~
        [ (sepalWidth e,petalWidth e) | e <- V.toList v, species e == s]
    $ plot_points_title .~ show s
    $ def
testData :: V.Vector Iris -> PlotPoints Double Double
testData v = plot_points_style .~ hollowCircles 5 0.2 (opaque black)
    $ plot_points_values .~
        [ (sepalWidth e,petalWidth e) | e <- V.toList v, testSet e]
    $ plot_points_title .~ "Test Set"
    $ def


