
<!--
	###############################################
	###############################################
	###############################################
-->

## Typklasse Enum

- sequentiell geordnete Typen (Es existieren Vorgänger und Nachfolger).

- Vorteil: die Typen können in Listenranges verwendet werden. 

Wichtigste Funktionen:

succ / pred 
:   Vorgänger / Nachfolger

        succ / pred :: a -> a

toEnum
:   Konvertiert `Int` zu `Enum`

        toEnum :: Int -> a

fromEnum
:   Konvertiert `Enum` zu `Int`.

        fromEnum :: a -> Int


## Typklasse Bounded

- `Bounded` Mitglieder haben eine obere und untere Grenze.

Es existieren für jedes Mitglied diese 2 konstanten Werte:

    minBound :: a
    maxBound :: a

Beispiel:

    ghci> minBound :: Int  
    -9223372036854775808 
    ghci> maxBound :: Char  
    '\1114111'  

## Typklasse Integral

    toInteger :: a -> Integer

Konvertierung in ein Integer

- Beispiel Unterschied div/quot und mod/rem

        Prelude> quot 8 (-3)
        -2
        Prelude> div 8 (-3)
        -3
        Prelude> mod 8 (-3)
        -1
        Prelude> rem 8 (-3)
        2

## Typklasse Floating

- **Floating** enthält nur Float und Double.

Funktionen und Konstanten von **Floating**

    pi :: a

$\pi$

    exp, sqrt, log :: a -> a 

Exponential-, Wurzel und Logatithmusfunktion (Basis 10)

    (**), logBase :: a -> a -> a

Potenz- und Logarithmusfunktion (beliebige Basis)

## Typklasse RealFrac

- **RealFrac** erweitert **Fraction**

Darunter sind Funktionen wie:

    truncate :: Integral b => a -> b 

Gibt den dichtesten Int zwischen 0 und a zurueck.

    round :: Integral b => a -> b 

Gibt den dichtesten Int von a zurueck.

    ceiling :: Integral b => a -> b 

Gibt den kleinst groesseren Int von a zurueck.

    floor :: Integral b => a -> b 

Gibt den groesst kleineren Int von a zurueck.

## Typklasse RealFrac

- Beispiel:

        Prelude> truncate (-2.6)
        -2
        Prelude> round (-2.6)
        -3
        Prelude> floor (-2.6)
        -3
        Prelude> ceiling (-2.6)
        -2

## Typklasse RealFloat

- **RealFloat** bietet effizienten, maschinenunabhaengigen Zugriff zu Teilen einer Floating-point Nummer nach IEEE Standard

- Funktionen wie:

- ueberpruefen ob Zahl in normalisierter oder denormalisierter Form dargestellt werden

- ueberpruefen, ob die Zahl unendlich oder NaN ist

- zurueckgeben von der Basis, dem Exponenten sowie den Signifikant zurueckgeben usw.

- usw.

## Lambdas in Funktionen

- Funktionsauswertungen auch anonym mit Lambdafunktionen möglich

- Aus

        addThree :: (Num a) => a -> a -> a -> a
        addThree x y z = x + y + z

kann

        addThree :: (Num a) => a -> a -> a -> a
        addThree = \x -> \y -> \z -> x + y + z

werden.

- Erstere Version zu bevorzugen (Lesbarkeit)

## wichtige Module: `Data.List`

- `Data.List` Modul zur Manipulation von Listen

## Funktionale

Ein Funktional $T$ ist eine Abbildung $T: V \rightarrow \mathbb{K}$ 
wobei $V$ ein $\mathbb{K}$-Vektorraum ist, häufig ein Funktionenraum. 

Bekanntes Beispiel: Dirac-Delta $\delta$

$f(0) = \int_{\Omega} f(x) \delta (x) dx$

Etwas subtiler: das begrenzte Integral selbst

$I_{a,b}[f] := \int_a^b f(x) dx$

TODO: Einleitung weiter
