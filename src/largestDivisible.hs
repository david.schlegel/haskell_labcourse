largestDivisible :: Integral a => a -> a -> a
largestDivisible x y = head (filter p [x,x-1..])
    where p z = z `mod` y == 0
