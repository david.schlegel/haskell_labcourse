{-# LANGUAGE BangPatterns #-}
import Data.Foldable
sumgrT :: Int -> [Int] -> Int
sumgrT x [] = []
sumgrT x (y:ys) = sumgrT (x+y) ys

sumgrT :: Int -> [Int] -> [Int]
sumgrT x [] = []
sumgrT x (y:ys) = (x+y) : sumgrT (x+y) ys

sumD :: [Int] -> [Int]
sumD [] = []
sumD (y:ys) = y + first $ sumD ys

main = do
    -- let b = sumgr 100000
    -- let !b = sumgrT 0 [1..100000]
    -- let !b = foldl' (+) 0 [1,2..1000000]
    -- let b = Data.Foldable.foldr' (+) 0 [1,2..100000]
    print b
