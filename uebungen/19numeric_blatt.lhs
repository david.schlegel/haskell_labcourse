
Aufgabe 1
---------

Löst näherungsweise die Fixpunktgleichung

$$x_f = e^{(-x_f)}$$

Benutzt als Startwert $x_f = 1.1$ baut einen Fehlerabbruch ein und iteriert maximal 1000 mal.
<!--

>fixpunkt :: Double -> Int -> Double
>fixpunkt z iter
>    | iter==1000 = z
>    | otherwise = fixpunkt (exp (-z)) (iter+1)
>
>aufgabe1= do
>    let iters=0
>    let start=1.1
>    let z = fixpunkt start iters
>    print z

-->







