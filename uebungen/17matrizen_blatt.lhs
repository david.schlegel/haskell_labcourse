% Übungen: Matrizen und Vektoren

<!--
>import Numeric.LinearAlgebra.Algorithms
>import Numeric.LinearAlgebra.Util
>import Numeric.Container

--Kompilieren geht, aber direkt ausführen geht wg. linking errors nicht.
>
>makeTuples:: [Double] -> [(Double,Double)] -> [(Double,Double)]
>makeTuples (a:range) tempRes
>    | a > 4.0 = (a,funct):tempRes
>    | otherwise = makeTuples range ((a,funct):tempRes)
>    where funct = (a^5)-(4*(a^4))-(10*a)^3+(40*a)^2+(9*a)-36
>    
>makeMatrix :: Int -> [Double] -> [Double] -> Matrix Double
>makeMatrix length (first:end) list
>    | end == [] = (length><length) (list ++ (take length (repeat first)))
>    | otherwise = makeMatrix length end (list ++ (take length (repeat first)))
>
>makeMultMatrix :: Matrix Double -> Matrix Double -> Int -> Int -> Int -> [Double] -> Matrix Double
>makeMultMatrix m1 m2 x y length res
>    | x==length-1 && y==length-1 = (length><length) (res ++ [mul])
>    | x==length-1 = makeMultMatrix m1 m2 0 (y+1) length (res ++ [mul])
>    | otherwise = makeMultMatrix m1 m2 (x+1) y length (res ++ [mul])
>    where mul = ((@@>) m1 (x,y))**((@@>) m2 (x,y))
>
>main = do
>   aufgabe2
>   aufgabe4
>   aufgabe5
-->

Aufgabe 1
---------

Berechnet die Nullstellen von dem Polynom

$$p(x)= x^5-4*x^4-10*x^3+40*x^2+9*x-36$$

Hinweis: Die Nullstellen sind die Eigenwerte (`eig`) der Begleitmatrix

<!--
$$\begin{pmatrix}
0 & 0 & 0 & 0 & 36 \\
1 & 0 & 0 & 0 & -9 \\
0 & 1 & 0 & 0 & -40 \\
0 & 0 & 1 & 0 & 10 \\
0 & 0 & 0 & 1 & 4 
\end{pmatrix}$$
-->

<!--
>aufgabe1 = do
>    let a = (5><5) [0,0,0,0,36,1,0,0,0,(-9),0,1,0,0,(-40),0,0,1,0,10,0,0,0,1,4::Double]
>    let (l,v) = eig a
>    print "Nullstellen sind:"
>    print l
>    print "Zeichnen:"
>    let res = makeTuples [(-3.0),(-2.9)..4] []
>    print res
>    -- plot X11 $ Data2D [Style Lines, Title "Plot"] [] res
-->

Aufgabe 2
---------

Erzeugt die (100 $\times$ 100) - Matrix

$$\begin{pmatrix}
2 & -1 & .. & .. & .. & 0  \\
-1 & 2 & -1 & .. & .. & .. \\
.. & .. & ..& .. & .. & .. \\
.. & .. & .. & .. & ..& .. \\
.. & .. & .. &-1 & 2 & -1 \\
0 & .. & .. & .. & -1 & 2
\end{pmatrix}$$


und berechnet ihre Determinante.

<!--
>help (i,j) 
>   | i == j = 2
>   | i == j-1 || i == j+1 = -1 
>   | otherwise = 0 
>aufgabe2 = do
>    print "Determinante der Aufgabe 2:"
>    let diagMatrix = buildMatrix 100 100 (help) :: Matrix Double
>    -- print diagMatrix
>    print $ det diagMatrix
-->

Aufgabe 3
---------

Schreibt eine Funktion, die zu einem gegebenen Vektor $x=(x_1,...,x_n)$ die Vandermonde Matrix

$$V:=\begin{pmatrix}
1 & x_1 & x_1^2 & .. & x_1^{n-1} \\
1 & x_2 & x_2^2 & .. & x_2^{n-1} \\
.. & .. & .. & .. & .. \\
1 & x_n & x_n^2 & ..  & x_n^{n-1}
\end{pmatrix}$$

berechnet und zurückgibt.

<!--
Tipp: $V=A.$^$B$ mit

$$A:=\begin{pmatrix}
x_1 & x_1 & ... & x_1 \\
x_2 & x_2 & ... & x_2 \\
.. & .. & .. & ..  \\
x_n & x_n & .. & x_n 
\end{pmatrix}$$

$$B:=\begin{pmatrix}
0 & 1 & ... & n-1 \\
0 & 1 & ... & n-1 \\
.. & .. & .. & ..  \\
0 & 1 & .. & n-1 
\end{pmatrix}$$
-->

<!--
>aufgabe3 = do
>    print "VandermondeMatrix fuer Liste [1,2,3,4]"
>    let q = [1,2,3,4]
>    let listLength = length q
>    let m1= makeMatrix listLength q []
>    --print m1
>    let b = [(fromIntegral x) | x<-[0..(listLength -1)]]
>    --print b
>    let m2= (listLength><listLength) (take (listLength^2) (cycle b))
>    --print m2
>    let m3 = makeMultMatrix m1 m2 0 0 listLength []
>    print m3
-->

Aufgabe 4
---------

- Erzeugt die (15×15) Hilbert-Matrix $H:=(h_{ij})$ mit $h_{ij}:=\frac{1}{i+j−1}$.
- Bestimmt die Determinante von H.
- Berechnet den Vektor $r:=(1,1,\ldots,1)^T$ das Matrix-Vektor-Produkt $y:=Hr$.
- Löst das Gleichungssystem $Hx=y$ sowohl mit einer LU-Zerlegung als auch mit `<\>` also einer Singulärwertzerlegung. Was fällt auf?
- Addiert die Werte der dritten Spalte.

<!-- 
>hilb n = build (n,n) (\i j -> 1/(i+j+1)) 
>aufgabe4 = do
>   let n = 15
>   print $ "Hilbert-Matrix der Groesse " ++ show n
>   let hilbert = hilb n :: Matrix Double
>   print hilbert
>   let r = n |> [1 | _ <- [1..]]
>   let y = hilbert <> r
>   print$  hilbert <\> y
>   print $ linearSolve hilbert (asColumn r)
>   -- print $ Prelude.sum((toLists hilbert)!!2)
-->

Aufgabe 5
---------

Berechnet die Frobenius-Norm

$$||A||_F := \sqrt{\sum_{i,j=1}^n a^2_{ij}}, A=(a_{ij}) \in \mathbb{R}^{nxn}$$

der Vandermonde Matrix mit gegebenen Vektor $[0,0.02..1]$.

<!--
>aufgabe5 = do
>    print "Frobeniusnorm der Vandermonde Matrix vander(0:0.02:1)"
>    let mm = makeMatrix 3 [0,0.02,1] []
>    let bb = [(fromIntegral x) | x<-[0..2]]
>    let mmm = (3><3) (take 9 (cycle bb))
>    let vander = makeMultMatrix mm mmm 0 0 3 []
>    print vander
>    let xaxa = makeMultMatrix vander vander 0 0 3 []
>    print xaxa
>    let qweqwew = sumElements xaxa
>    print qweqwew
>    let frobenius= sqrt qweqwew
>    print frobenius
-->

Aufgabe 6
---------

Berechnet die Eigenwerte und Eigenvektoren der Matrix

$$A=\begin{pmatrix}
30 & 1 & 2 & 3 \\
4 & 15 & -4 & -2 \\
-1 & 0 & 3 & 5 \\
-3 & 5 & 0 & -1
\end{pmatrix}$$

Bestimmt ebenfalls die QR- und LU-Zerlegungen.


<!--
>aufgabe6 = do
>    let wut = (4><4) [30,1,2,3,4,15,-4,-2,-1,0,3,5,-3,5,0,1::Double]
>    print "Aufgabe 5 Matrix"
>    print wut
>    let (eigenwerte,eigenvektoren) = eig wut
>    print "Eigenwerte und Eigenvektoren"
>    print eigenwerte
>    print eigenvektoren
>    let (l,u,per,s) = lu wut
>    print "lower,upper,permutation matrix und signatur"
>    print l
>    print u
>    print per
>    print s
>    let (q,r) = qr wut
>    print "unitary, upper triangular"
>    print q
>    print r
-->

Aufgabe 7
---------

Welche der Vektoren 
$$v_1=\left(\begin{array}{c} 1\\2\\3\\4\\5 \end{array}\right),\quad v_2=\left(\begin{array}{c}-1\\27\\26\\1\\-27 \end{array}\right),\quad v_3=\left(\begin{array}{c} 160\\-48\\112\\-160\\48 \end{array}\right),\quad v_4=\left(\begin{array}{c} 120\\234\\-23\\-43\\29 \end{array}\right)$$ 

sind zueinander orthogonal?


<!--
v1 = vector([1,2,3,4,5])
v2 = vector([-1,27,26,1,-27])
v3 = vector([160,-48,112,-160,48])
v4 = vector([120,234,-23,-43,29])
-->
