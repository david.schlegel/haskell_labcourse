#!/usr/bin/python3

import json
import io
import sys

nbfile = io.open(sys.argv[1], "r", encoding='utf-8')
jsf = json.load (nbfile)
nbfile.close()

jsf["cells"] = list(filter (lambda x: x["cell_type"] != "code", jsf["cells"]))

nbfileout = io.open("../exercises/" + sys.argv[1].replace("_sol",""), "w", encoding='utf-8')
json.dump (jsf,nbfileout,indent=2)
nbfileout.close()
