modFibs :: Integral a => a -> a
modFibs a = tailFibs 0 1 0 a

tailFibs:: Integral a => a -> a -> a -> a -> a
tailFibs prev1 prev2 start end
    | start == end = next
    | otherwise = tailFibs next prev1 (start+1) end
    where next = prev1 + prev2

main = do
    let g = modFibs 1000
    print g
