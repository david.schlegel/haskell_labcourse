
.PHONY: all
all: $(patsubst %.md,%.pdf,$(wildcard *.md))

PDPARAM = -f markdown+lhs -V lang=ngerman --listings --slide-level 2  
PDCALL = pandoc $(PDPARAM)
TEXOPT = -t beamer --template=hs  -V theme:Madrid

.PHONY: zipsolpdf
zipvorpdf: 
	zip -9 lectures.zip $(wildcard *.pdf)

%.pdf: %.md hs.beamer
		$(PDCALL) $(TEXOPT) $< -o $@

haskell.tex: haskell.md hs.beamer
	$(PDCALL) $(TEXOPT) -o haskell.tex

haskell_s.html: haskell.md hs.beamer
	$(PDCALL) -s -t slidy --webtex -o haskell.html 

haskell_r.html: haskell.md hs.beamer
	$(PDCALL) -s -t revealjs --webtex -o haskell.html 

.PHONY: clean
clean:
	rm -f haskell.pdf haskell.html haskell.tex
	rm -f haskell.aux haskell.log haskell.out haskell.vrb
.PHONY: view
view: haskell.pdf
	zathura haskell.pdf
