{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Monads II\n",
    "\n",
    "## The State monad\n",
    "\n",
    "Imperative languages have global variables, Haskell has similar functinality with *state monads*."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "newtype State s a = State { runState :: s -> (a, s) }"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- `State s a` is a value of type `a`, obtained by a stateful computation with state of type `s` (e.g. a record type holding all \"global\" variables).\n",
    "\n",
    "- `runState` takes an initial state and outputs the value *and a new state*.\n",
    "\n",
    "- Compare this to the intuition for the `IO` monad in the previous lecture: `IO == State RealWorld`.\n",
    "\n",
    "- For every `s`, `State s` is a monad:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Control.Monad\n",
    "instance Functor (State s) where\n",
    "    fmap = liftM\n",
    "instance Applicative (State s) where\n",
    "    pure = return\n",
    "    (<*>) = ap\n",
    "\n",
    "instance Monad (State s) where\n",
    "    return x = State $ \\s -> (x, s)\n",
    "    x >>= f = State $ \\s -> let (x', s') = runState x s\n",
    "                            in runState (f x') s'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**`get`** gets the current state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "get :: State s s\n",
    "get = State $ \\s -> (s, s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**`gets`** is a variation that applies a function to the state before returning. Useful e.g. if the state is a record type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "gets :: (s -> a) -> State s a\n",
    "gets f = State $ \\s -> (f s, s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**`put`** sets a new state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "put :: s -> State s ()\n",
    "put s = State $ const ((), s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**`modify`** applies a function to the state"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "modify :: (s -> s) -> State s ()\n",
    "modify f = State $ \\s -> ((), f s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**`evalState`**, **`execState`** short forms for computing only the resulting value / resulting state, resp."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "evalState :: State s a -> s -> a\n",
    "evalState m s = fst $ runState m s\n",
    "\n",
    "execState :: State s a -> s -> s\n",
    "execState m s = snd $ runState m s"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: Turing machine\n",
    "\n",
    "- infinitely long tape with letters (here: integers), initialized to 0\n",
    "- read/write head that in each step:\n",
    "    - reads a letter\n",
    "    - writes a new letter\n",
    "    - moves left, right or stays\n",
    "- the machine has an internal state indexed by (a finite subset of the) integers that can influence decisions and change in each step\n",
    "- the machine stops if a special internal state (the final state) is reached"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "-- for clarity\n",
    "type Letter = Integer\n",
    "type MState = Integer\n",
    "-- state type for the State monad, includes state of tape and internal state of machine\n",
    "data Turing = Turing { left :: [Letter]\n",
    "                     , cur :: Letter\n",
    "                     , right :: [Letter]\n",
    "                     , machineState :: MState\n",
    "                     }\n",
    "-- where to move next                     \n",
    "data Move = GoLeft | GoRight | Stay\n",
    "-- the transition function of the machine\n",
    "type TuringFunc = MState -> Letter -> (MState, Letter, Move)\n",
    "-- this is conceptually the same as\n",
    "--   type TuringFunc = State (MState, Letter) Move\n",
    "-- but we do not use that here\n",
    "              \n",
    "turingInit :: Turing\n",
    "turingInit = Turing { left = repeat 0, right = repeat 0, machineState = 0 }\n",
    "\n",
    "runTuring :: TuringFunc -> MState -> State Turing Letter\n",
    "runTuring func final = do\n",
    "    c <- gets cur\n",
    "    ms <- gets machineState\n",
    "    if ms == final\n",
    "    then return c\n",
    "    else do\n",
    "         let (ms', c', mv) = func ms c\n",
    "         modify $ \\t -> t { cur = c', machineState = ms' }\n",
    "         c <- gets cur\n",
    "         l <- gets left\n",
    "         r <- gets right\n",
    "         case mv of\n",
    "             Stay -> return ()\n",
    "             GoLeft -> modify $ \\t -> t { left = tail l\n",
    "                                        , cur = head l\n",
    "                                        , right = c:r\n",
    "                                        }\n",
    "             GoRight -> modify $ \\t -> t { left = c:l\n",
    "                                         , cur = head r\n",
    "                                         , right = tail r\n",
    "                                         }\n",
    "         runTuring func final\n",
    "\n",
    "execTuring :: TuringFunc -> MState -> Turing\n",
    "execTuring func final = execState (runTuring func final) turingInit\n",
    "\n",
    "evalTuring :: TuringFunc -> MState -> Letter\n",
    "evalTuring func final = evalState (runTuring func final) turingInit"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`State` is defined in `Control.Monad.State`. The actual definition is more involved than the one above (more below).\n",
    "\n",
    "Restart the kernel before evaluating the next cell."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {},
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import Control.Monad.State\n",
    ":info State"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Reader and Writer"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Defined in `Control.Monad.Reader` and `Control.Monad.Writer`.\n",
    "\n",
    "- Reader can be viewed as a read-only version of the State monad.\n",
    "\n",
    "    ```haskell\n",
    "    newtype Reader r a = { runReader :: r -> a }\n",
    "    ```\n",
    "\n",
    "- Writer can be used e.g. for logging or incrementally building up a result\n",
    "\n",
    "    ```haskell\n",
    "    newtype Writer w a = { runWriter :: (a, w) }\n",
    "    ```\n",
    "\n",
    "    `Writer w` is a monad only if `w` is a `Monoid`; `(>>=)` uses the monoid structure (exercise: how?) to combine, e.g. append, the result.\n",
    "\n",
    "- Some operations (exercise: what do they do, how can they be defined?)\n",
    "\n",
    "    ```haskell\n",
    "    tell :: w -> Writer w ()\n",
    "    listen :: Writer w a -> Writer w (a, w)\n",
    "    ask :: Reader r r\n",
    "    local :: (r -> r) -> Reader r a -> Reader r a\n",
    "    ```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Nesting monads: Monad Transformers\n",
    "\n",
    "### Task\n",
    "\n",
    "*Read lines of input unil an empty line is encountered. Return a list of all lines. Do this using a `Writer`.*\n",
    "\n",
    "How should `Writer` and `IO` be combined?\n",
    "\n",
    "#### `Writer [IO String] ()`\n",
    "\n",
    "- Need to use `sequence . execWriter` to get from `[IO String]` to `IO [String]` at the end.\n",
    "- Before that, we do not have access to the `String` wrapped in `IO` (since the action has not actually been performed yet) ...\n",
    "- ... so it is not possible to test for the empty string.\n",
    "\n",
    "#### `Writer (IO [String]) ()`\n",
    "- `IO [a]` is not a `Monoid` (but it is easy to write an instance).\n",
    "- `tell` expects an `IO [String]`, which we read using `getLine :: IO String` like\n",
    "\n",
    "```haskell\n",
    "do x <- getLine\n",
    "   return [x]\n",
    "```\n",
    "\n",
    "- The `String` `x` is only available inside this `do` block ...\n",
    "- ... so testing for emptiness can not influence the surrounding function.\n",
    "\n",
    "#### `IO (Writer [String] ())`\n",
    "\n",
    "- Even without emptiness test, this won't work:\n",
    "\n",
    "```haskell\n",
    "readAndTell :: IO (Writer [String] ())\n",
    "readAndTell = do \n",
    "   x <- getLine\n",
    "   return $ do\n",
    "       tell [x]\n",
    "       readAndTell -- error: we can not perform IO inside the Writer!\n",
    "```\n",
    "        \n",
    "- If we performe the recursive call outside the `Writer`, we loose the line we just `tell`'d\n",
    "\n",
    "```haskell\n",
    "readAndTell :: IO (Writer [String] ())\n",
    "readAndTell = do \n",
    "   x <- getLine\n",
    "   return $ tell [x]\n",
    "   readAndTell -- works, but [x] is gone\n",
    "```\n",
    "    \n",
    "- We have to explictly carry around the `Writer` to keep the state:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 56,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Control.Monad.Writer\n",
    "readAndTell' :: Writer [String] () -> IO (Writer [String] ())\n",
    "readAndTell' w = do\n",
    "    x <- getLine\n",
    "    if x == \"\" then return w\n",
    "    else readAndTell' $ w >> tell [x]\n",
    "    \n",
    "readAndTell :: IO [String]\n",
    "readAndTell = liftM execWriter $ readAndTell' (return ())"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- However, this is no better than explicitly passing the `[String]` list around"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Better solution using `WriterT`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 144,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "readAndTell' :: WriterT [String] IO ()\n",
    "readAndTell' = do\n",
    "    x <- lift getLine\n",
    "    when (x /= \"\") $ do\n",
    "        tell [x]\n",
    "        readAndTell'\n",
    "\n",
    "readAndTell :: IO [String]\n",
    "readAndTell = execWriterT readAndTell'"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- `WriterT [String]` is similar to `Writer [String]`, but wraps another `Monad` instead of a plain type."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>/*\n",
       "Custom IHaskell CSS.\n",
       "*/\n",
       "\n",
       "/* Styles used for the Hoogle display in the pager */\n",
       ".hoogle-doc {\n",
       "    display: block;\n",
       "    padding-bottom: 1.3em;\n",
       "    padding-left: 0.4em;\n",
       "}\n",
       ".hoogle-code {\n",
       "    display: block;\n",
       "    font-family: monospace;\n",
       "    white-space: pre;\n",
       "}\n",
       ".hoogle-text {\n",
       "    display: block;\n",
       "}\n",
       ".hoogle-name {\n",
       "    color: green;\n",
       "    font-weight: bold;\n",
       "}\n",
       ".hoogle-head {\n",
       "    font-weight: bold;\n",
       "}\n",
       ".hoogle-sub {\n",
       "    display: block;\n",
       "    margin-left: 0.4em;\n",
       "}\n",
       ".hoogle-package {\n",
       "    font-weight: bold;\n",
       "    font-style: italic;\n",
       "}\n",
       ".hoogle-module {\n",
       "    font-weight: bold;\n",
       "}\n",
       ".hoogle-class {\n",
       "    font-weight: bold;\n",
       "}\n",
       "\n",
       "/* Styles used for basic displays */\n",
       ".get-type {\n",
       "    color: green;\n",
       "    font-weight: bold;\n",
       "    font-family: monospace;\n",
       "    display: block;\n",
       "    white-space: pre-wrap;\n",
       "}\n",
       "\n",
       ".show-type {\n",
       "    color: green;\n",
       "    font-weight: bold;\n",
       "    font-family: monospace;\n",
       "    margin-left: 1em;\n",
       "}\n",
       "\n",
       ".mono {\n",
       "    font-family: monospace;\n",
       "    display: block;\n",
       "}\n",
       "\n",
       ".err-msg {\n",
       "    color: red;\n",
       "    font-style: italic;\n",
       "    font-family: monospace;\n",
       "    white-space: pre;\n",
       "    display: block;\n",
       "}\n",
       "\n",
       "#unshowable {\n",
       "    color: red;\n",
       "    font-weight: bold;\n",
       "}\n",
       "\n",
       ".err-msg.in.collapse {\n",
       "  padding-top: 0.7em;\n",
       "}\n",
       "\n",
       "/* Code that will get highlighted before it is highlighted */\n",
       ".highlight-code {\n",
       "    white-space: pre;\n",
       "    font-family: monospace;\n",
       "}\n",
       "\n",
       "/* Hlint styles */\n",
       ".suggestion-warning { \n",
       "    font-weight: bold;\n",
       "    color: rgb(200, 130, 0);\n",
       "}\n",
       ".suggestion-error { \n",
       "    font-weight: bold;\n",
       "    color: red;\n",
       "}\n",
       ".suggestion-name {\n",
       "    font-weight: bold;\n",
       "}\n",
       "</style><span class='get-type'>Writer [String] :: * -> *</span>"
      ],
      "text/plain": [
       "Writer [String] :: * -> *"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/html": [
       "<style>/*\n",
       "Custom IHaskell CSS.\n",
       "*/\n",
       "\n",
       "/* Styles used for the Hoogle display in the pager */\n",
       ".hoogle-doc {\n",
       "    display: block;\n",
       "    padding-bottom: 1.3em;\n",
       "    padding-left: 0.4em;\n",
       "}\n",
       ".hoogle-code {\n",
       "    display: block;\n",
       "    font-family: monospace;\n",
       "    white-space: pre;\n",
       "}\n",
       ".hoogle-text {\n",
       "    display: block;\n",
       "}\n",
       ".hoogle-name {\n",
       "    color: green;\n",
       "    font-weight: bold;\n",
       "}\n",
       ".hoogle-head {\n",
       "    font-weight: bold;\n",
       "}\n",
       ".hoogle-sub {\n",
       "    display: block;\n",
       "    margin-left: 0.4em;\n",
       "}\n",
       ".hoogle-package {\n",
       "    font-weight: bold;\n",
       "    font-style: italic;\n",
       "}\n",
       ".hoogle-module {\n",
       "    font-weight: bold;\n",
       "}\n",
       ".hoogle-class {\n",
       "    font-weight: bold;\n",
       "}\n",
       "\n",
       "/* Styles used for basic displays */\n",
       ".get-type {\n",
       "    color: green;\n",
       "    font-weight: bold;\n",
       "    font-family: monospace;\n",
       "    display: block;\n",
       "    white-space: pre-wrap;\n",
       "}\n",
       "\n",
       ".show-type {\n",
       "    color: green;\n",
       "    font-weight: bold;\n",
       "    font-family: monospace;\n",
       "    margin-left: 1em;\n",
       "}\n",
       "\n",
       ".mono {\n",
       "    font-family: monospace;\n",
       "    display: block;\n",
       "}\n",
       "\n",
       ".err-msg {\n",
       "    color: red;\n",
       "    font-style: italic;\n",
       "    font-family: monospace;\n",
       "    white-space: pre;\n",
       "    display: block;\n",
       "}\n",
       "\n",
       "#unshowable {\n",
       "    color: red;\n",
       "    font-weight: bold;\n",
       "}\n",
       "\n",
       ".err-msg.in.collapse {\n",
       "  padding-top: 0.7em;\n",
       "}\n",
       "\n",
       "/* Code that will get highlighted before it is highlighted */\n",
       ".highlight-code {\n",
       "    white-space: pre;\n",
       "    font-family: monospace;\n",
       "}\n",
       "\n",
       "/* Hlint styles */\n",
       ".suggestion-warning { \n",
       "    font-weight: bold;\n",
       "    color: rgb(200, 130, 0);\n",
       "}\n",
       ".suggestion-error { \n",
       "    font-weight: bold;\n",
       "    color: red;\n",
       "}\n",
       ".suggestion-name {\n",
       "    font-weight: bold;\n",
       "}\n",
       "</style><span class='get-type'>WriterT [String] :: (* -> *) -> * -> *</span>"
      ],
      "text/plain": [
       "WriterT [String] :: (* -> *) -> * -> *"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    ":kind Writer [String]\n",
    ":kind WriterT [String]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- The result `WriterT [String] IO` is a `Monad` again. \n",
    "\n",
    "- Inside this monad, `tell` can be used, but also `IO` performed using `lift` to access the wrapped monad.\n",
    "\n",
    "### Monad transformer type classes\n",
    "\n",
    "- `lift` belongs to the `MonadTrans` class\n",
    "\n",
    "```haskell\n",
    "class MonadTrans t where\n",
    "    lift :: Monad m => m a -> t m a\n",
    "```\n",
    "\n",
    "- `WriterT w` is a `MonadTrans`\n",
    "\n",
    "```haskell\n",
    "instance Monoid w => MonadTrans (WriterT w)\n",
    "\n",
    "```\n",
    "\n",
    "- `WriterT w m` is also a monad\n",
    "\n",
    "```haskell\n",
    "instance (Monoid w, Monad m) => Monad (WriterT w m)\n",
    "```\n",
    "\n",
    "Similar things hold for `ReaderT`, `StateT`, `MaybeT`, ... Moreover,\n",
    "\n",
    "```haskell\n",
    "type Writer w = WriterT w Identity\n",
    "```\n",
    "\n",
    "### Stacked transformers\n",
    "\n",
    "- Since transformed monads are monads themselves, they can be transformed again:\n",
    "\n",
    "```haskell\n",
    "type App = ReaderT AppConfig (WriterT AppLog (StateT AppState IO))\n",
    "```\n",
    "\n",
    "- There is no `IOT`, so `IO` has to be at the bottom of the stack if it is needed.\n",
    "\n",
    "- Operations need to be lifted using `lift` for the first level, `lift . lift` for the second, etc...\n",
    "\n",
    "- For `IO`, there is also `liftIO` which lifts an `IO` action from the bottom of the stack.\n",
    "\n",
    "### Getting rid of `lift`\n",
    "\n",
    "Many of the standard monad transformers have their functionality split out into type classes, e.g. instead of\n",
    "\n",
    "```haskell\n",
    "tell :: w -> Writer w ()\n",
    "```\n",
    "    \n",
    "or\n",
    "    \n",
    "    \n",
    "```haskell\n",
    "tell :: Monad m => w -> WriterT w m ()\n",
    "```\n",
    "    \n",
    "the signature is"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 59,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/html": [
       "<style>/*\n",
       "Custom IHaskell CSS.\n",
       "*/\n",
       "\n",
       "/* Styles used for the Hoogle display in the pager */\n",
       ".hoogle-doc {\n",
       "    display: block;\n",
       "    padding-bottom: 1.3em;\n",
       "    padding-left: 0.4em;\n",
       "}\n",
       ".hoogle-code {\n",
       "    display: block;\n",
       "    font-family: monospace;\n",
       "    white-space: pre;\n",
       "}\n",
       ".hoogle-text {\n",
       "    display: block;\n",
       "}\n",
       ".hoogle-name {\n",
       "    color: green;\n",
       "    font-weight: bold;\n",
       "}\n",
       ".hoogle-head {\n",
       "    font-weight: bold;\n",
       "}\n",
       ".hoogle-sub {\n",
       "    display: block;\n",
       "    margin-left: 0.4em;\n",
       "}\n",
       ".hoogle-package {\n",
       "    font-weight: bold;\n",
       "    font-style: italic;\n",
       "}\n",
       ".hoogle-module {\n",
       "    font-weight: bold;\n",
       "}\n",
       ".hoogle-class {\n",
       "    font-weight: bold;\n",
       "}\n",
       "\n",
       "/* Styles used for basic displays */\n",
       ".get-type {\n",
       "    color: green;\n",
       "    font-weight: bold;\n",
       "    font-family: monospace;\n",
       "    display: block;\n",
       "    white-space: pre-wrap;\n",
       "}\n",
       "\n",
       ".show-type {\n",
       "    color: green;\n",
       "    font-weight: bold;\n",
       "    font-family: monospace;\n",
       "    margin-left: 1em;\n",
       "}\n",
       "\n",
       ".mono {\n",
       "    font-family: monospace;\n",
       "    display: block;\n",
       "}\n",
       "\n",
       ".err-msg {\n",
       "    color: red;\n",
       "    font-style: italic;\n",
       "    font-family: monospace;\n",
       "    white-space: pre;\n",
       "    display: block;\n",
       "}\n",
       "\n",
       "#unshowable {\n",
       "    color: red;\n",
       "    font-weight: bold;\n",
       "}\n",
       "\n",
       ".err-msg.in.collapse {\n",
       "  padding-top: 0.7em;\n",
       "}\n",
       "\n",
       "/* Code that will get highlighted before it is highlighted */\n",
       ".highlight-code {\n",
       "    white-space: pre;\n",
       "    font-family: monospace;\n",
       "}\n",
       "\n",
       "/* Hlint styles */\n",
       ".suggestion-warning { \n",
       "    font-weight: bold;\n",
       "    color: rgb(200, 130, 0);\n",
       "}\n",
       ".suggestion-error { \n",
       "    font-weight: bold;\n",
       "    color: red;\n",
       "}\n",
       ".suggestion-name {\n",
       "    font-weight: bold;\n",
       "}\n",
       "</style><span class='get-type'>tell :: forall w (m :: * -> *). MonadWriter w m => w -> m ()</span>"
      ],
      "text/plain": [
       "tell :: forall w (m :: * -> *). MonadWriter w m => w -> m ()"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    ":t tell"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "and there is an instance\n",
    "\n",
    "```haskell\n",
    "instance (Monoid w, Monad m) => MonadWriter w (WriterT w m)\n",
    "```\n",
    "\n",
    "There are also instances like\n",
    "\n",
    "```haskell\n",
    "instance MonadWriter w m => MonadWriter w (ReaderT r m)\n",
    "```\n",
    "\n",
    "Therefore, `ReaderT r (WriterT w IO)` is a `MonadWriter w` and can use `tell` directly, without `lift`.\n",
    "\n",
    "The same holds for classes like\n",
    "- `MonadReader`\n",
    "- `MonadState`\n",
    "- `MonadIO` (which provides `liftIO` described above)\n",
    "\n",
    "Some classes are missing for some reason, like e.g. `MonadMaybe`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Example: MaybeT\n",
    "\n",
    "*Read two numbers from stdin and print their sum, or \"Error\" if an input is not a number.*\n",
    "\n",
    "Use `readMaybe :: Read a => String -> Maybe a` from `Text.Read`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 118,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Text.Read hiding (lift)\n",
    "\n",
    "getMaybe :: Read a => IO (Maybe a)\n",
    "getMaybe = do\n",
    "    x <- getLine\n",
    "    return $ readMaybe x\n",
    "    \n",
    "readAndAdd = do\n",
    "    x <- getMaybe\n",
    "    y <- getMaybe\n",
    "    let z = liftM2 (+) x y\n",
    "    maybe (putStrLn \"Error\") print z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Works, but if the first input is an error, the second one is still read.\n",
    "- We work in the `IO` monad and do not have automatic termination on error that `Maybe` implements.\n",
    "- Back to manual checking:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 135,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Data.Maybe\n",
    "    \n",
    "readAndAdd' = do\n",
    "    x <- getMaybe\n",
    "    z <- if isJust x then do\n",
    "             y <- getMaybe\n",
    "             return $ liftM2 (+) x y\n",
    "         else return Nothing\n",
    "    maybe (putStrLn \"Error\") print z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We essentially have to implement `Maybe`'s `(>>=)` on top of `IO`. That is precisely what `MaybeT IO` is for."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 149,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Control.Monad.Trans.Maybe\n",
    "import Text.Read hiding (lift)\n",
    "\n",
    "-- we need to wrap the return value of readMaybe using\n",
    "--   MaybeT :: m (Maybe a) -> MaybeT m a\n",
    "readMaybeT :: (Monad m, Read a) => String -> MaybeT m a\n",
    "readMaybeT = MaybeT . return . readMaybe\n",
    "\n",
    "getMaybeT :: Read a => MaybeT IO a\n",
    "getMaybeT = do\n",
    "    x <- liftIO getLine\n",
    "    readMaybeT x\n",
    "    \n",
    "readAndAddT = do\n",
    "    z <- runMaybeT $ do \n",
    "             -- here we can use IO *and* automatically break\n",
    "             -- if reading x fails\n",
    "             x <- getMaybeT\n",
    "             y <- getMaybeT\n",
    "             return $ x + y\n",
    "    maybe (putStrLn \"Error\") print z"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Implementation of `MaybeT`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data MaybeT m a = MaybeT { runMaybeT :: m (Maybe a) }\n",
    "\n",
    "import Control.Monad\n",
    "instance Monad m => Functor (MaybeT m) where\n",
    "    fmap = liftM\n",
    "instance Monad m => Applicative (MaybeT m) where\n",
    "    pure = return\n",
    "    (<*>) = ap\n",
    "\n",
    "instance Monad m => Monad (MaybeT m) where\n",
    "    x >>= f = MaybeT $ do\n",
    "        x' <- runMaybeT x\n",
    "        case x' of\n",
    "            Just a -> runMaybeT $ f a\n",
    "            Nothing -> return Nothing\n",
    "\n",
    "    return = MaybeT . return . Just"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Compare with `Maybe`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "instance Monad Maybe where\n",
    "    x >>= f = case x of\n",
    "        Just a -> f a\n",
    "        Nothing -> Nothing\n",
    "\n",
    "    return = Just"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Except for wrapping (`MaybeT`) and unwrapping (`runMaybeT`), the main differences are the additional `return` and the `(>>=)` (which is implicit in `<-` in the `do`-block) inside the wrapped monad `m`.\n",
    "\n",
    "Finally, make `MaybeT` a `MonadTrans`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Control.Monad.Trans\n",
    "instance MonadTrans MaybeT where\n",
    "    -- lift :: Monad m => m a -> MaybeT m a\n",
    "    lift x = MaybeT $ do\n",
    "        x' <- x\n",
    "        return (Just x')"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Random numbers\n",
    "\n",
    "- Functions returning random numbers are not *referentially transparent*: they return a different result each time they are called.\n",
    "\n",
    "- Many languages carry around a some state in a global variable that is implicitly updated on generating a number.\n",
    "\n",
    "- Conceptually, if `g` is the global state, random numbers are generated by a function `g -> (a, g)` (note the similarity to the `State` monad).\n",
    "\n",
    "### `RandomGen`\n",
    "\n",
    "The typeclass `RandomGen` in `System.Random` provides an interface for this:\n",
    "\n",
    "```haskell\n",
    "class RandomGen where\n",
    "    next :: g -> (Int, g)\n",
    "    genRange :: g -> (Int, Int)\n",
    "    split :: g -> (g, g)\n",
    "```\n",
    "\n",
    "- There is an implicit global `RandomGen` of type `StdGen`, which can be accessed from the `IO` monad:\n",
    "\n",
    "**getStdGen**:  <br />\n",
    "    \n",
    "```haskell\n",
    "getStdGen :: IO StdGen\n",
    "```\n",
    "\n",
    "**setStdGen**: <br />\n",
    "\n",
    "```haskell\n",
    "setStdGen :: StdGen -> IO ()\n",
    "```\n",
    "\n",
    "**getStdRandom**: <br />\n",
    "\n",
    "```haskell\n",
    "getStdRandom :: forall a. (StdGen -> (a, StdGen)) -> IO a\n",
    "```\n",
    "\n",
    "**mkStdGen**: <br />\n",
    "\n",
    "```haskell\n",
    "mkStdGen :: Int -> StdGen\n",
    "```\n",
    "\n",
    "### `Random`\n",
    "\n",
    "The typeclass `Random` is an interface for types that can be generated randomly. All number types implement this typeclass. Some functions:\n",
    "\n",
    "**randomR**: <br />\n",
    "generate a random numbers in a given range\n",
    "```haskell\n",
    "randomR :: forall a g. (RandomGen g, Random a) => (a, a) -> g -> (a, g)\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0.6655656716728674"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "import System.Random\n",
    "getStdRandom $ randomR (0::Double, 1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**randomRs**: <br />\n",
    "iterated version: generate an infinte list of numbers\n",
    "```haskell\n",
    "randomRs :: forall a g. (RandomGen g, Random a) => (a, a) -> g -> [a]\n",
    "```\n",
    "\n",
    "**randomRIO**: <br />\n",
    "convenience function using the global StdGen\n",
    "```haskell\n",
    "randomRIO :: forall a. Random a => (a, a) -> IO a\n",
    "```\n",
    "\n",
    "**random** etc: <br />\n",
    "use type dependent default ranges\n",
    "\n",
    "```haskell\n",
    "random :: forall a g. (RandomGen g, Random a) => g -> (a, g)\n",
    "randoms :: forall a g. (RandomGen g, Random a) => g -> [a]\n",
    "randomIO :: forall a. Random a => IO a\n",
    "```\n",
    "\n",
    "### Other random distributions\n",
    "\n",
    "... are provided by packages/modules, e.g."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Data.Random.Normal"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```haskell\n",
    "normal :: (Floating a, RandomGen g, Random a) => g -> (a, g)\n",
    "normals :: (Floating a, RandomGen g, Random a) => g -> [a]\n",
    "normalIO :: (Floating a, Random a) => IO a\n",
    "normal' :: (Floating a, RandomGen g, Random a) => (a, a) -> g -> (a, g)\n",
    "normals' :: (Floating a, RandomGen g, Random a) => (a, a) -> g -> [a]\n",
    "normalIO' :: (Floating a, Random a) => (a, a) -> IO a\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Provides normally distributed random numbers, either ('-versions) with given (mean, variance), or using a default of (0, 1)."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
