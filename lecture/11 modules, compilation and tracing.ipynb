{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "\n",
    "# Modules, compilation and tracing\n",
    "\n",
    "## Modules\n",
    "\n",
    "... are collections of functions, types and type classes.\n",
    "\n",
    "- Advantage: Sufficiently generic functions can be reused by other applications\n",
    "- (Almost) all languages provide similar mechanisms: no need to reimplement everything\n",
    "- Modules can expose only a subset of functions $\\Rightarrow$ hide implementation details\n",
    "\n",
    "## Haskell standard library\n",
    "\n",
    "The standard library is split into modules\n",
    "\n",
    "- list manipulation, complex numbers, ...\n",
    "- Core module **Prelude** contains almost all functions used so far. The Prelude is loaded automatically.\n",
    "\n",
    "## Importing entire modules\n",
    "\n",
    "```haskell\n",
    "import <module_name>\n",
    "```\n",
    "\n",
    "All functions and types exported by the module will be available.\n",
    "\n",
    "*Example*: Importing `Debug.HTrace`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Debug.HTrace"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`Debug.HTrace` containts a function `Debug.HTrace.htrace` to trace function calls that can be used after the import:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "+\n",
       "7"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "htrace \"+\" 3+4"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Importing parts of modules\n",
    "\n",
    "Import statements as above import all objects exported by the module $\\Rightarrow$ name conflicts are possible. Solutions:\n",
    "\n",
    "- Importing *only* specific objects:\n",
    "\n",
    "```haskell\n",
    "import <module_name> (object_name)\n",
    "```\n",
    "\n",
    "- Importing everything *excluding* specific objects:\n",
    "\n",
    "```haskell\n",
    "import <module_name> hiding (object_name)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Qualified imports\n",
    "\n",
    "Import into seperate namespace:\n",
    "\n",
    "```haskell\n",
    "import qualified <module_name> (object_name) as <name>\n",
    "```\n",
    "\n",
    "- \"`(object_name)`\" and \"`as <name>`\" are optional\n",
    "\n",
    "*Examples*:\n",
    "\n",
    "```haskell\n",
    "import qualified Debug.HTrace\n",
    "```\n",
    "\n",
    "$\\Rightarrow$ the function `htrace` has to be called as `Debug.HTrace.htrace`\n",
    "\n",
    "```haskell\n",
    "import qualified Debug.HTrace as H\n",
    "```\n",
    "\n",
    "$\\Rightarrow$ the function `htrace` has to be called as `H.htrace`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Stack\n",
    "\n",
    "- **stack** is a tool for developing Haskell projects, including:\n",
    "    - installing ghc\n",
    "    - installing packages (project-specific or globally)\n",
    "    - building, testing and benchmarking projects\n",
    "\n",
    "- You can [install](http://docs.haskellstack.org/en/stable/install_and_upgrade.html) it to bootstrap a complete Haskell environment on your machine (this is the recommended way)\n",
    "\n",
    "- The original tool was called **cabal** but is deprecated (due to producing dependency problems, the so called cabal-hell)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Managing projects with stack\n",
    "\n",
    "- Stack is based on setting up isolated environments for each project.\n",
    "\n",
    "- To set up a new project named \"foo\":\n",
    "\n",
    "        $ stack new foo       # create directory and relevant files\n",
    "        \n",
    "        $ cd foo\n",
    "        \n",
    "        $ stack setup         # download ghc if necessary\n",
    "        \n",
    "        $ stack build         # build the project\n",
    "        \n",
    "        $ stack exec foo-exe  # execute it\n",
    " \n",
    "- In the generated directory structure\n",
    "    - *app/* is the directory for source files of executables\n",
    "    - *src/* is the directory for library source files\n",
    "    - *foo.cabal* contains information on the project, add dependencies in the *build-depends:* section"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Using stack\n",
    "\n",
    "- Modules should be installed package-wise via its dependencies\n",
    "\n",
    "- Executables may be installed globally (to ~/.local/bin), e.g.\n",
    "\n",
    "        $ stack install hlint\n",
    "        \n",
    "        $ ~/.local/bin/hlint\n",
    "\n",
    "- Modules can be looked up on [Hoogle](http://www.haskell.org/hoogle/), [Hackage](http://hackage.haskell.org/) or [Stackage](http://www.stackage.org)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Hackage vs. Stackage\n",
    "\n",
    "- The default package repository is *Hackage*.\n",
    "- *Stackage* is an alternative that provides sets of packages that are tested to work together nicely.\n",
    "- **Disadvantage**: some packages may be missing, some may be slightly older.\n",
    "\n",
    "- stack uses Stackage snapshots as far as possible, but can fall back to Hackage if necessary."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Example: sieve\n",
    "\n",
    "We create a project to see how this works and how to compile Haskell code. To demonstrate this, we recall the function `sieve`:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sieve :: Integral a => [a] -> [a]\n",
    "sieve [] = []\n",
    "sieve (x:xs) = x : sieve [ y | y <- xs, y `mod` x /= 0 ]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- First create a new project named *sieve*:\n",
    "\n",
    "    $ stack new sieve\n",
    "        \n",
    "    $ cd sieve"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- To make Haskell code run outside the notebook interface, it has to be put into a file with extension `.hs`. We use atom for editing the main project file\n",
    "\n",
    "    $ atom app/Main.hs"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the notebook interface, we can simply type"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "metadata": {
    "collapsed": false,
    "scrolled": true
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[2,3,5,7,11,13,17,19]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "sieve [2..20]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In a source file, however, one must add instead"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "main :: IO ()\n",
    "main = print (sieve [2..20])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In the notebook interface, expressions are evaluated directly, and their result is displayed in the notebook. Executables do not do that. Instead, they need an *entry point*, a function called `main`.\n",
    "\n",
    "A template is already in the *Main.hs* file, we only need to substitude `someFunc` and of course add the `sieve` function.\n",
    "\n",
    "To output more than one result, use"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "main :: IO ()\n",
    "main = do\n",
    "    print (sieve [2..20])\n",
    "    print (3*4)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note**: Both the type `IO ()` of `main` and the `do` syntax are core concepts of Haskell and will be explained in much more detail later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- This file can then be executed directly\n",
    "\n",
    "        $ stack runhaskell Main.hs\n",
    "    \n",
    "    or compiled into an executable\n",
    "    \n",
    "        $ stack ghc Main.hs\n",
    "        \n",
    "    The executable is named like the source file (without extension) and can be run with\n",
    "        \n",
    "        $ ./Main\n",
    "\n",
    "\n",
    "**Note**: Enable warnings: command line parameters for GHC\n",
    "\n",
    "- *-Wall*: enable all warnings\n",
    "- *-fwarn-incomplete-patterns*: warn on incomplete pattern matching\n",
    "\n",
    "        $ stack ghc -- -Wall -fwarn-incomplete-patterns Main.hs\n",
    "        \n",
    "You can also set up default parameters in the cabal file instead."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tracing function calls\n",
    "\n",
    "Tracing helps visualizing the steps, results and call depth of nested functions (e.g. inside a recursion).\n",
    "\n",
    "*Motivation*:\n",
    "\n",
    "- Understand the order of processing of functions.\n",
    "- See which operation is performed and which is left out due to laziness.\n",
    "\n",
    "*Example*: `htrace` from `Debug.HTrace`\n",
    "\n",
    "```haskell\n",
    "htrace :: String -> a -> a\n",
    "```\n",
    "\n",
    "- `htrace str x` prints the string `str` and returns `x`.\n",
    "- Calls to `htrace` that happen during the evaluation of `x` are indented.\n",
    "- Well suited to visualize nesting and recursion depth.\n",
    "\n",
    "### Tracing `sumr`, `sumtr` and `sumtrs`\n",
    "\n",
    "Same definition as before, with `htrace` inserted:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 65,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sumr :: Int -> Int\n",
    "sumr n \n",
    "    | htrace (\"sumr \" ++ show n) False = undefined\n",
    "    | n == 0 = 0\n",
    "    | otherwise = htrace \"+\" (n + sumr (htrace \"-\" (n-1)))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The first pattern guard is a trick to trace the function call itself:\n",
    "\n",
    "- The pattern guard is tested on every call,\n",
    "- ... prints the tracing output,\n",
    "- ... but always fails since it returns `False`. \n",
    "- The value is therefore never used and does not have to be anything useful. `undefined` is used because it can have every type, so type checking will succeed here. Any fixed `Int` value would have also worked."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 62,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "sumr 3\n",
       "+\n",
       "  sumr   -\n",
       "2\n",
       "  +\n",
       "    sumr     -\n",
       "1\n",
       "    +\n",
       "      sumr       -\n",
       "0\n",
       "6"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "sumr 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Tracing without printing the argument:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 63,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sumr :: Int -> Int\n",
    "sumr n \n",
    "    | htrace \"sumr\" False = undefined\n",
    "    | n == 0 = 0\n",
    "    | otherwise = htrace \"+\" (n + sumr (htrace \"-\" (n-1)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 64,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "sumr\n",
       "+\n",
       "  sumr\n",
       "  -\n",
       "  +\n",
       "    sumr\n",
       "    -\n",
       "    +\n",
       "      sumr\n",
       "      -\n",
       "6"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "sumr 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "**Note**:\n",
    "- In the first version, the subtraction gets evaluated between printing `\"sumr \"` and `show n`, as this is the first time the value `n` is needed!\n",
    "- $\\Rightarrow$ Be careful with evaluating arguments in the call to `htrace` itself. You might change the order of execution!\n",
    "- GHC can sometimes optimize the order of execution or remove laziness (*strictness analysis*), thereby also changing the tracing output. To prevent that, turn off optimization with\n",
    "\n",
    "        $ ghc -O0 <file>.hs\n",
    "        \n",
    "    when compiling source files.\n",
    "        \n",
    "Tracing `sumtr` (without printing `m` and `n` in order to keep laziness):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 57,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sumtr :: Int -> Int\n",
    "sumtr n = sumtr' 0 n\n",
    "    where sumtr' m n \n",
    "            | htrace \"sumtr'\" False = undefined\n",
    "            | n == 0 = m\n",
    "            | otherwise = sumtr' (htrace \"+\" (m+n)) (htrace \"-\" (n-1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 58,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "sumtr'\n",
       "sumtr'\n",
       "-\n",
       "sumtr'\n",
       "-\n",
       "sumtr'\n",
       "-\n",
       "+\n",
       "  +\n",
       "    +\n",
       "6"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "sumtr 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(Note the tail recursion and the nested calls to `(+)` when evaluating the returned *thunk*.)\n",
    "\n",
    "Finally, `sumtrs`. Here, we can `show` `n` and `m` since they are evaluated anyway due to strictness:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 72,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    ":ext BangPatterns\n",
    "\n",
    "sumtrs :: Int -> Int\n",
    "sumtrs n = sumtrs' 0 n\n",
    "    where sumtrs' !m !n \n",
    "            | htrace (\"sumtrs' \" ++ show m ++ \" \" ++ show n) False = undefined\n",
    "            | n == 0 = m\n",
    "            | otherwise = sumtrs' (htrace \"+\" (m+n)) (htrace \"-\" (n-1))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 73,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "sumtrs' 0 3\n",
       "+\n",
       "-\n",
       "sumtrs' 3 2\n",
       "+\n",
       "-\n",
       "sumtrs' 5 1\n",
       "+\n",
       "-\n",
       "sumtrs' 6 0\n",
       "6"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "sumtrs 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Introduction to profiling\n",
    "\n",
    "Profiling is the analysis of running time and memory usage of compiled programs. It has to be enabled by compiler options and cannot be used in the notebook:\n",
    "\n",
    "- *`-prof`*: activates profiling\n",
    "- *`-fprof-auto`*: profiles all bindings except for inlines\n",
    "- *`-rtsopts`*: enable runtime parameters on the generated executable\n",
    "\n",
    "The executable can be called with additional profiling options between *`+RTS`* and *`-RTS`*:\n",
    "\n",
    "- *`-p`*: activates running time and memory profiling (generates `.prof` file)\n",
    "- *`-hc`*: activates heap profiling (generates `.hp` file)\n",
    "- *`-sstderr`*: outputs some global statistics, e.g. garbage collection\n",
    "\n",
    "Heap profiles can be converted to `.ps` by\n",
    "\n",
    "    $ hp2ps -c <name>.hp\n",
    "    \n",
    "*Example*\n",
    "\n",
    "    $ ghc -prof -fprof-auto -rtsopts factorial.hs\n",
    "\n",
    "    $ ./factorial +RTS -p -hc -sstderr -RTS\n",
    "    \n",
    "**Note:** We call the compiler directly here. However, in a stack-managed project you should call ghc via *$ stack ghc -- -prof -fprof-auto -rtsopts <file.hs>*\n",
    "    \n",
    "- Output on `stderr` (excerpt):\n",
    "\n",
    "```\n",
    "29,110,430,776 bytes allocated in the heap\n",
    "    36,285,328 bytes copied during GC\n",
    "     5,676,336 bytes maximum residency (51 sample(s))\n",
    "        87,712 bytes maximum slop\n",
    "            10 MB total memory in use (0 MB lost due to fragmentation)\n",
    "```\n",
    "                    \n",
    "- factorial.prof (excerpt):\n",
    "\n",
    "```\n",
    "COST CENTRE MODULE  %time %alloc\n",
    "---- ------ ------ ------ ------\n",
    "facNE       Main     44.7   31.2\n",
    "facR.fac'   Main     23.7   34.3\n",
    "facRf.fac'  Main     23.0   34.3\n",
    "main        Main      8.6    0.3\n",
    "```\n",
    "\n",
    "- Heap plot:\n",
    "\n",
    "![Heap Plot](factorial_heap.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Writing modules\n",
    "\n",
    "Modules are kept in files of their own that should be named like the module. In a project managed by stack, modules reside in the *src/* directory.\n",
    "\n",
    "MyModule.hs:\n",
    "\n",
    "---\n",
    "```haskell\n",
    "module MyModule\n",
    "    ( name1\n",
    "    , name2\n",
    "    ) where\n",
    "    \n",
    "name1 = ...\n",
    "\n",
    "name2 = ...\n",
    "\n",
    "name3 = ...\n",
    "```\n",
    "\n",
    "---\n",
    "\n",
    "The module is imported using\n",
    "\n",
    "```haskell\n",
    "import MyModule\n",
    "```\n",
    "\n",
    "After this, `name1` and `name2` are available. `name3` was not exported and is private to the module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
