{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    " # Minimal surfaces\n",
    "\n",
    " ## Task\n",
    "\n",
    "Given a closed curve $\\gamma$ in $\\mathbb{R}^3$, determine the (smoothly\n",
    "embedded) surface of minimal area that has $\\gamma$ as its boundary.\n",
    "\n",
    " ## Formalities\n",
    "\n",
    "- $\\Omega \\subset \\mathbb{R}^2$ is the (open) parameter set with boundary\n",
    "$\\partial\\Omega$.\n",
    "\n",
    "- $f \\colon \\Omega \\to \\mathbb{R}^3$ is the embedding (i.e. parametrization),\n",
    "its image $f(\\Omega)$ is a surface in $\\mathbb{R}^3$ with boundary\n",
    "$f(\\partial\\Omega)$. (Technically, $f$ should be $C^2$).\n",
    "\n",
    "- $Df \\colon \\Omega \\to \\mathbb{R}^{3 \\times 2}$ is the Jacobian, i.e.\n",
    "\n",
    "$$(Df(x))_{ij} = \\frac{\\partial f_i(x)}{\\partial x_j}.$$\n",
    "\n",
    "- $g = (Df)^T Df \\colon \\Omega \\to \\mathbb{R}^{2 \\times 2}$ is the metric tensor\n",
    "of the surface.\n",
    "\n",
    "- The area of the surface $f(\\Omega)$ is\n",
    "\n",
    "$$A(f) = \\int\\limits_\\Omega \\sqrt{\\det g(x)} \\,dx.$$\n",
    "\n",
    "- $\\sqrt{\\det g}$ is the *surface element*. We also write $\\sqrt{\\det g} \\,dx =\n",
    "d\\sigma$.\n",
    "\n",
    "We are searching for an $f$ such that $f(\\partial\\Omega) = \\gamma$ and\n",
    "\n",
    "$$A(f) = \\min\\left\\{ A(\\tilde{f}) \\colon \\tilde{f} \\colon \\Omega \\to\n",
    "\\mathbb{R}^3, \\tilde{f}(\\partial\\Omega) = \\gamma \\right\\}$$\n",
    "\n",
    " ## Basic idea\n",
    "\n",
    "Minimize $A$ using gradient descent:\n",
    "\n",
    "$$f_{n+1} = f_n - \\epsilon \\nabla A(f_n), \\qquad \\epsilon: \\text{ step size}$$\n",
    "\n",
    "The derivative of $A$ at $f$ is\n",
    "\n",
    "$$DA(f)(h) = \\frac{\\partial}{\\partial t} A(f + th) |_{t = 0}$$\n",
    "\n",
    "where $h \\colon \\Omega \\to \\mathbb{R}^3$ is an \"infinitesimal shift\" with\n",
    "$h(\\partial\\Omega) = 0$ (i.e. not changing the boundary).\n",
    "\n",
    "A tedious but simple calculation using [Jacobi's\n",
    "formula](https://en.wikipedia.org/wiki/Jacobi%27s_formula) for the derivative of\n",
    "the determinant shows\n",
    "\n",
    "$$DA(f)(h) = \\int\\limits_\\Omega \\operatorname{tr}(g^{-1} (Df)^T Dh) \\,d\\sigma.$$\n",
    "\n",
    "Partial integration (boundary terms vanish due to $h(\\partial\\Omega) = 0$) leads\n",
    "to\n",
    "\n",
    "$$DA(f)(h) = - \\int\\limits_\\Omega \\left( \\sum_{ijk} \\frac{1}{\\sqrt{\\det g}}\n",
    "\\frac{\\partial}{\\partial x_i} g^{-1}_{ij} \\frac{\\partial}{\\partial x_j} f_k\n",
    "\\right) h_k \\,d\\sigma.$$\n",
    "\n",
    "so (in a suitable sense), the gradient of $A$ is\n",
    "\n",
    "$$\\nabla A(f) = -\\sum_{ijk} \\frac{1}{\\sqrt{\\det g}} \\frac{\\partial}{\\partial\n",
    "x_i} g^{-1}_{ij} \\frac{\\partial}{\\partial x_j} f = \\Delta_{f(\\Omega)} f$$\n",
    "\n",
    "which is the *Laplace-Beltrami operator* of the surface $f(\\Omega)$, applied to\n",
    "$f$ itself.\n",
    "\n",
    " ## Imports\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import Control.Monad (zipWithM_, foldM)\n",
    "import Data.Function (on)\n",
    "import Data.List (foldl')\n",
    "import qualified Data.Map.Strict as M\n",
    "import Data.Maybe (fromJust)\n",
    "import qualified Data.Set as S\n",
    "import Numeric.Container ((<>), (<.>), norm2, buildMatrix)\n",
    "import Numeric.LinearAlgebra.HMatrix hiding ((<>), CGState, cgx, cgp, cgr, cgr2)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    " ## Discrete surfaces\n",
    "\n",
    "Triangular mesh\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data Mesh = Mesh { vertices :: Matrix Double\n",
    "                 , faces :: [(Int, Int, Int)]\n",
    "                 }\n",
    "            deriving Show\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    "`vertices` is an $n \\times 3$ matrix, each element of `faces` is a triple of\n",
    "indices into the `points` matrix defining a triangle.\n",
    "\n",
    "On the triangular mesh, define basis functions $\\phi_p$ for every vertex $p$\n",
    "(i.e. every row of `vertices`) with\n",
    "\n",
    "- $\\phi_p(p) = 1$\n",
    "\n",
    "- $\\phi_p(q) = 0$ for every vertex $q \\not= p$\n",
    "\n",
    "- $\\phi_p$ is linear on each triangle\n",
    "\n",
    "A triangular mesh is not a smooth surface. It can be shown, however, that the\n",
    "gradient of the surface functional is still given by the (discrete)\n",
    "Laplace-Beltrami operator, the matrix elements of which are $\\Delta_{p q} = 0$\n",
    "if $p$ is on the boundary of the mesh, and\n",
    "\n",
    "$$\\Delta_{p q} = - \\sum_{T \\text{ triangle}} \\int\\limits_T (\\nabla \\phi_p(x))^T\n",
    "\\nabla \\phi_q(x) dx$$\n",
    "\n",
    "otherwise. Gradient descent now becomes\n",
    "\n",
    "$$\\text{vertices}_{n+1} = \\text{vertices}_n - \\epsilon \\Delta_n \\cdot\n",
    "\\text{vertices}_n,$$\n",
    "\n",
    "where $\\Delta_n$ is the Lapalce operator on the mesh defined by\n",
    "$\\text{vertices}_n$.\n",
    "\n",
    " ## Computing the Laplace operator\n",
    "\n",
    "Given vertices `x1`, `x2`, `x3`, compute the integral above for $p$ and $q$\n",
    "running over these vertices (all other integrals vanish).\n",
    "\n",
    "Some preliminaries:\n",
    "\n",
    "- The three basis functions, taken as a map from the triangle to $\\mathbb{R}^3$,\n",
    "map bijectively onto the standard simplex\n",
    "\n",
    "$$S = \\left\\{ x \\in \\mathbb{R}^3 \\colon x \\geq 0, \\sum_i x_i = 1 \\right\\}$$\n",
    "\n",
    "- The inverse of this map is just the matrix $(x1 \\,\\, x2 \\,\\, x3)$. On the\n",
    "other hand, this provides a way of computing the basis functions as the inverse\n",
    "of that matrix. (Note that it might be necessary to shift the triangle to ensure\n",
    "the matrix is not singular).\n",
    "\n",
    "- $\\nabla \\phi_p$ is constant on each triangle, since $\\phi_p$ is linear. The\n",
    "gradients of the basis of the functions in $\\mathbb{R}^3$ are again simply given\n",
    "by (the rows of) the matrix $(x1 \\,\\, x2 \\,\\, x3)^{-1}$.\n",
    "\n",
    "- The *surface gradient* is the $\\mathbb{R}^3$ gradient, projected onto the\n",
    "triangle.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "laplaceForTriangle :: (Vector Double, Vector Double, Vector Double) -> Matrix Double\n",
    "laplaceForTriangle (x1, x2, x3) = area `scale` (grads <> tr grads)\n",
    "    where -- Area and surface normal of the triangle\n",
    "          a = cross (x1 - x3) (x2 - x3)\n",
    "          area = 1/2 * norm2 a\n",
    "          normal = (1 / norm2 a) `scale` a\n",
    "          -- Shift triangle to ensure non-singular matrix. All vertices are\n",
    "          -- shifted by the same amount in normal direction. The choice of\n",
    "          -- coefficient is a bit technical and not very interesting...\n",
    "          x = fromColumns [x1, x2, x3]\n",
    "          coeff = 1 - det x / (a <.> a)\n",
    "          invBasis = x + coeff `scale` fromColumns [a, a, a]\n",
    "          -- Compute basis functions\n",
    "          -- basis = inv $ fromColumns [x1, x2, x3]\n",
    "          basis = inv invBasis\n",
    "          -- Projection onto triangle. Gradients are the rows of basis,\n",
    "          -- so we project on the right.\n",
    "          grads = basis - basis <> outer normal normal\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    "Given a mesh, we want to compute the (sparse) Laplace operator. For this, we\n",
    "need to construct an `AssocMatrix`. Recall its definition:\n",
    "\n",
    "```haskell\n",
    "type AssocMatrix = [((Int, Int), Double)]\n",
    "```\n",
    "\n",
    "First, turn the Laplace for a single triangle (given by a tuple of indices) into\n",
    "an `AssocMatrix`:\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "toAssocMatrix :: (Int, Int, Int) -> Matrix Double -> AssocMatrix\n",
    "toAssocMatrix (i, j, k) mat = [ ((a, b), mat ! ai ! bi)\n",
    "                              | (a, ai) <- l\n",
    "                              , (b, bi) <- l ]\n",
    "    where l = zip [i, j, k] [0, 1, 2]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    "The Laplace operator contains an element for every edge of the mesh. Since\n",
    "triangles can share edges, we need a way to sum all elements of the\n",
    "`AssocMatrix` that have identical indices.\n",
    "\n",
    "Use a lookup table (`Data.Map`):\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "uniquifyAssocMatrix :: AssocMatrix -> AssocMatrix\n",
    "uniquifyAssocMatrix = M.assocs . foldl' ins M.empty\n",
    "    where ins acc (k, v) = M.insertWith (+) k v acc\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    "The Laplace is then computed by iterating over all faces. (Note that we do not\n",
    "handle the boundary properly yet.)\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "laplace :: Mesh -> AssocMatrix\n",
    "laplace Mesh { vertices = vs, faces = fs } = uniquifyAssocMatrix elems\n",
    "  where getTriangle (i, j, k) = (vs!i, vs!j, vs!k)\n",
    "        elems = [ elm | face <- fs\n",
    "                      , elm <- toAssocMatrix face . laplaceForTriangle $\n",
    "                               getTriangle face ]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    "We also need a way to determine the boundary: boundary edges are the ones that\n",
    "only belong to a single triangle.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sortTuple :: Ord a => (a, a) -> (a, a)\n",
    "sortTuple (i, j) = if i < j then (i, j) else (j, i)\n",
    "\n",
    "edges :: Mesh -> [(Int, Int)]\n",
    "edges mesh = [ sortTuple edge\n",
    "             | (i, j, k) <- faces mesh\n",
    "             , edge <- [(i, j), (j, k), (k, i)] ]\n",
    "\n",
    "countOccurences :: Ord a => [a] -> [(a, Int)]\n",
    "-- same idea as uniquifyAssocMatrix\n",
    "countOccurences = M.assocs . foldl' ins M.empty . map (\\x -> (x, 1))\n",
    "    where -- This is the same ins as above, point-free\n",
    "          ins = flip . uncurry $ M.insertWith (+)\n",
    "\n",
    "boundary :: Mesh -> S.Set Int\n",
    "boundary mesh = S.fromList [ idx\n",
    "                           | ((i, j), count) <- countOccurences $ edges mesh\n",
    "                           , count == 1\n",
    "                           , idx <- [i, j]\n",
    "                           ]\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    "Finally, gradient descent can be implemented.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "-- Hmatrix does not provide products between a sparse and a full matrix.\n",
    "-- This implementation here is not very efficient, but simple.\n",
    "mult :: GMatrix -> Matrix Double -> Matrix Double\n",
    "mult gm = fromColumns . map (gm !#>) . toColumns\n",
    "\n",
    "gradientDescent :: Double -> Mesh -> [Mesh]\n",
    "gradientDescent stepsize mesh = iterate step mesh\n",
    "    where bndry = boundary mesh\n",
    "          onBoundary ((i, _), _) = i `S.member` bndry\n",
    "          step msh@Mesh { vertices = vs } =\n",
    "              msh { vertices = vs - stepsize `scale` (lapl `mult` vs) }\n",
    "              -- Sparse matrix would be nicer, but does not work: hmatrix tries\n",
    "              -- to guess the size of the matrix, but gets it wrong sometimes:\n",
    "              -- where lapl = mkSparse . filter (not . onBoundary) . laplace $ mesh\n",
    "              where lapl = mkDense . toDense .\n",
    "                           filter (not . onBoundary) . laplace $ msh\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    " ## Gradient Descent in $H^1$\n",
    "\n",
    "Without proof: The method can be improved by taking the gradient in the Sobolev\n",
    "space $H^1$, which leads to the iteration\n",
    "\n",
    "$$\\text{vertices}_{n+1} = \\text{vertices}_n - \\epsilon u_n.$$\n",
    "\n",
    "where $u_n$ solves\n",
    "\n",
    "$$\\Delta_n u = \\Delta_n \\text{vertices}_n$$\n",
    "\n",
    "on inner points, and\n",
    "\n",
    "$$u = 0$$\n",
    "\n",
    "on the boundary. We solve the equation using the Conjugate Gradient method.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "data CGState = CGState { cgx :: Matrix Double\n",
    "                       , cgp :: Matrix Double\n",
    "                       , cgr :: Matrix Double\n",
    "                       , cgr2 :: Double }\n",
    "\n",
    "cg :: GMatrix -> Matrix Double -> [CGState]\n",
    "cg mat rhs = iterate cgStep (CGState zeros rhs rhs (rhs `mdot` rhs))\n",
    "    where mdot = dot `on` flatten\n",
    "          zeros = buildMatrix (rows rhs) (cols rhs) (const 0)\n",
    "          cgStep (CGState x p r r2) = CGState x' p' r' r2'\n",
    "              where matp = mat `mult` p\n",
    "                    alpha = r2 / (p `mdot` matp)\n",
    "                    x' = x + alpha `scale` p\n",
    "                    r' = r - alpha `scale` matp\n",
    "                    r2' = r' `mdot` r'\n",
    "                    p' = r' + (r2' / r2) `scale` p\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "gradientDescentH1 :: Double -> Mesh -> [Mesh]\n",
    "gradientDescentH1 stepsize mesh = iterate step mesh\n",
    "    where bndry = boundary mesh\n",
    "          rowOnBoundary ((i, _), _) = i `S.member` bndry\n",
    "          rowOrColOnBoundary ((i, j), _) = any (`S.member` bndry) [i, j]\n",
    "          -- See `gradientDescent' for note on why mkSparse does not work\n",
    "          mkLaplace pred = mkDense . toDense . filter pred\n",
    "          cgStoppingRule state = cgr2 state < 1e-4\n",
    "          step msh@Mesh { vertices = vs } =\n",
    "              msh { vertices = vs - stepsize `scale` delta }\n",
    "              where lapl0 = laplace msh\n",
    "                    rhs = mkLaplace (not . rowOnBoundary) lapl0 `mult` vs\n",
    "                    lapl = mkLaplace (not . rowOrColOnBoundary) lapl0\n",
    "                    delta = cgx . head . filter cgStoppingRule $ cg lapl rhs\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    " ## Mesh construction\n",
    "\n",
    "Starting from a simple, coarse base mesh, we iteratively construct finer meshes\n",
    "by adding vertices in the center of all edges and splitting each triangle into 4\n",
    "smaller ones.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "refineMesh :: Mesh -> Mesh\n",
    "refineMesh mesh = Mesh { vertices = newvs, faces = newfs }\n",
    "    where (newvs, mids) = foldl' addMidpoint (vertices mesh, M.empty) $\n",
    "                          edges mesh\n",
    "          addMidpoint (vs, ms) edge@(i, j) =\n",
    "              let vs' = vs\n",
    "                        ===\n",
    "                        fromRows [(1/2) `scale` (vs!i + vs!j)]\n",
    "                  ms' = M.insert (sortTuple edge) (rows vs) ms\n",
    "              in (vs', ms')\n",
    "          getMid edge = fromJust $ M.lookup (sortTuple edge) mids\n",
    "          newfs = foldl' refineFace [] $ faces mesh\n",
    "          refineFace fs (i, j, k) = let mi = getMid (j, k)\n",
    "                                        mj = getMid (k, i)\n",
    "                                        mk = getMid (i, j)\n",
    "                                    in (i, mj, mk) :\n",
    "                                       (mi, j, mk) :\n",
    "                                       (mi, mj, k) :\n",
    "                                       (mi, mj, mk) :\n",
    "                                       fs\n",
    "\n",
    "baseMesh :: Mesh\n",
    "baseMesh =\n",
    "    Mesh { vertices = (6><3) [ 0, 0, 0\n",
    "                             , 1, 0, 0\n",
    "                             , 1, 0, 1\n",
    "                             , 1, 1, 1\n",
    "                             , 0, 1, 1\n",
    "                             , 0, 1, 0\n",
    "                             ],\n",
    "           faces = [ (0, 1, 5)\n",
    "                   , (1, 2, 5)\n",
    "                   , (2, 4, 5)\n",
    "                   , (2, 3, 4)\n",
    "                   ]\n",
    "         }\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    " ## The main part\n",
    "\n",
    "Save a mesh to files that can be read by the `vis.py` script. Call it like\n",
    "\n",
    "    vis.py --type triangles --base <prefix>\n",
    "\n",
    "where `<prefix>` is the `String` argument to `saveMesh`.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "saveMesh :: String -> Mesh -> IO ()\n",
    "saveMesh prefix mesh = do\n",
    "  zipWithM_ sav names columns\n",
    "  writeFile (prefix ++ \"-t.txt\") triStr\n",
    "    where sav fpath = saveMatrix fpath \"%f\"\n",
    "          names = map (prefix ++) [\"-x.txt\", \"-y.txt\", \"-z.txt\"]\n",
    "          columns = map asColumn $ toColumns $ vertices mesh\n",
    "\n",
    "          fmtFace (i, j, k) = show i ++ \" \" ++ show j ++ \" \" ++ show k\n",
    "          triStr = unlines $ map fmtFace $ faces mesh\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "hidden": false
   },
   "source": [
    "Functions to process a list of meshs as generated by\n",
    "\n",
    "```haskell\n",
    "gradientDescent :: Double -> Mesh -> [Mesh]\n",
    "```\n",
    "\n",
    "Iterations are stopped when the distance between two meshs (in $L^2$-norm) are\n",
    "below some tolerance. For each iteration, the distance to the preceding mesh is\n",
    "printed.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "process :: Monad m => [a] -> (a -> m ()) -> m a\n",
    "process xs f = foldM (\\_ x -> f x >> return x) undefined xs\n",
    "\n",
    "takeUntil :: (a -> Bool) -> [a] -> [a]\n",
    "takeUntil _ [] = []\n",
    "takeUntil predicate (x:xs)\n",
    "    | predicate x = [x]\n",
    "    | otherwise   = x : takeUntil predicate xs\n",
    "\n",
    "processMeshs :: Double -> [Mesh] -> IO Mesh\n",
    "processMeshs tol meshs = do\n",
    "  let meshs' = takeUntil ((< tol) . snd)\n",
    "              [ (cur, norm_Frob $ vertices prev - vertices cur)\n",
    "              | (cur, prev) <- zip (tail meshs) meshs ]\n",
    "  fmap (fst . snd) $ process (zip [(1::Int)..] meshs') $ \\(n, (_, diff)) ->\n",
    "      putStrLn $ show n ++ \" \" ++ show diff\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "main :: IO ()\n",
    "main = do\n",
    "  let mesh = iterate refineMesh baseMesh !! 3\n",
    "  let tol = 1e-2\n",
    "\n",
    "  saveMesh \"initial\" mesh\n",
    "\n",
    "  putStrLn \"Gradient descent\"\n",
    "  processMeshs tol (gradientDescent 0.1 mesh) >>= saveMesh \"minsurf\"\n",
    "\n",
    "  putStrLn \"Gradient descent H1\"\n",
    "  processMeshs tol (gradientDescentH1 1 mesh) >>= saveMesh \"minsurf-h1\"\n"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
