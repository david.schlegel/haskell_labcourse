{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "\n",
    "# Tail recursion\n",
    "\n",
    "A function $f$ is tail-recursive if and only if:\n",
    "\n",
    "$f(x) = \\left\\{\\begin{array}{ll} b(x) & \\text{if $B(x)$} \\\\ f(s(x)) &\\text{otherwise} \\end{array}\\right.$.\n",
    "\n",
    "- $b$ and $B$: base case, $s$: arbitrary function\n",
    "\n",
    "Compared to general recursion: $f(s(x))$ instead of $r(f, x)$ $\\Rightarrow$ the recursive call is the *last* operation (tail position)\n",
    "\n",
    "Tail recursive functions are equivalent to iterative loops.\n",
    "\n",
    "**General recursion**: Memory increases linearly with recursion depth, since every function call requires memory (stack frame, return address, local variables)\n",
    "\n",
    "**Tail recursion**: Execution can return directly to the caller, the local stack frame can be cleared before calling the function.\n",
    "\n",
    "## Guarded recursion\n",
    "\n",
    "... is a type of recursion where the recursive call is performed inside a *lazy* expression that can produce a result without actually evaluating the recursive call.\n",
    "\n",
    "- Typical examples are *data constructors*.\n",
    "\n",
    "*Example*: partial sums of a list"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "partialSums list = partialSums' list 0\n",
    "    where partialSums' [] s = [s]\n",
    "          partialSums' (x:xs) s = s : partialSums' xs (x+s)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "- Here, `(:)` is the data constructor (*list constructor*).\n",
    "- Same pattern works for custom types (explained later).\n",
    "- The recursive call is performed only when the second or higher elements are needed."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "0"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    },
    {
     "data": {
      "text/plain": [
       "[0,1]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "ps = partialSums [1,2,3,4,5]\n",
    "head ps -- recursive call to partialSums' still not evaluated\n",
    "take 2 ps -- now it is"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Compiler optimization and lazy evaluation\n",
    "\n",
    "- The compiler can optimize some functions to prevent creating stack frames (**TODO** using option `-O2`)\n",
    "\n",
    "- Guarded recursion can be almost as efficient as tail recursion.\n",
    "\n",
    "- *Downside*: lazy evaluation creates *thunks* (pieces of unevaluated code and local variables) that use up stack space ...\n",
    "\n",
    "- ... even for tail recursion\n",
    "\n",
    "- *strictness analysis* can prevent some of these issues by treating expressions strict automatically.\n",
    "\n",
    "## Example: Sum - recursion\n",
    "\n",
    "**Task**: Compute $s_n := \\sum_{i=1}^n i$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "sumr :: Int -> Int\n",
    "sumr 0 = 0\n",
    "sumr n = n + sumr (n-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`sumr` is not tail recursive.\n",
    "\n",
    "### Classic (non-lazy) evaluation\n",
    "\n",
    "... would proceed this way:\n",
    "\n",
    "```haskell\n",
    "sumr 3 = 3 + sumr 2\n",
    "    sumr 2 = 2 + sumr 1\n",
    "        sumr 1 = 1 + sumr 0\n",
    "           sumr 0 = 0\n",
    "        sumr 1 = 1 + 0 = 1\n",
    "    sumr 2 = 2 + 1 = 3\n",
    "sumr 3 = 3 + 3 = 6\n",
    "```\n",
    "\n",
    "allocating a stack frame for each nested call.\n",
    "\n",
    "### Using laziness\n",
    "\n",
    "Evaluation of `sumr 3` yields a *thunk*, i.e. an unevaluated expression:\n",
    "\n",
    "```haskell\n",
    "sumr 3\n",
    "==> 3 + sumr 2\n",
    "```\n",
    "\n",
    "Here, `==>` means \"evaluates to\".\n",
    "\n",
    "This is **all** that is performed directly! Only if we ask for the value (e.g. to print it), the *thunk* has to be evaluated.\n",
    "\n",
    "For this, the `(+)` expression has to be evaluated. But`(+)` is **not** lazy, so it needs to evaluate its arguments:\n",
    "\n",
    "```haskell\n",
    "==> 3 + (2 + sumr 1)\n",
    "```\n",
    "\n",
    "The same holds for the nested `(+)`:\n",
    "\n",
    "```haskell\n",
    "==> 3 + (2 + (1 + sumr 0))\n",
    "```\n",
    "\n",
    "... and again for the third `(+)`. This time `sumr 0` returns a value, not a *thunk*:\n",
    "\n",
    "```haskell\n",
    "==> 3 + (2 + (1 + 0))\n",
    "==> 3 + (2 + 1)\n",
    "==> 3 + 3\n",
    "==> 6\n",
    "```\n",
    "\n",
    "**Note:**\n",
    "\n",
    "- No stack frame is kept *inside* `sumr`...\n",
    "- ... but still inside `(+)` due to its strict behaviour.\n",
    "\n",
    "\n",
    "---\n",
    "\n",
    "Compare to a similar function with a lazy operation (`(:)` instead of `(+)`):"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "listDownFrom :: Int -> [Int]\n",
    "listDownFrom 0 = []\n",
    "listDownFrom n = n : listDownFrom (n-1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "metadata": {
    "collapsed": false
   },
   "outputs": [
    {
     "data": {
      "text/plain": [
       "[3,2,1]"
      ]
     },
     "metadata": {},
     "output_type": "display_data"
    }
   ],
   "source": [
    "listDownFrom 3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Evaluation of `listDownFrom 3` again returns a *thunk*:\n",
    "\n",
    "```haskell\n",
    "listDownFrom 3\n",
    "==> 3 : listDownFrom 2\n",
    "```\n",
    "\n",
    "If we want the full list, everything is as above. But if we only want a part of it:,\n",
    "\n",
    "```haskell\n",
    "head (listDownFrom 3)\n",
    "==> head (3 : listDownFrom 2)\n",
    "==> 3\n",
    "```\n",
    "\n",
    "we can return without performing the (guarded) recursion.\n",
    "\n",
    "## Example: Sum - tail recursion\n",
    "\n",
    "Transform `sumr` into a tail recursive function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sumtr :: Int -> Int\n",
    "sumtr n = sumtr' 0 n\n",
    "    where sumtr' m 0 = m\n",
    "          sumtr' m n = sumtr' (m+n) (n-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "`sumtr'` is tail recursive. The sum is evaluated *before* calling `sumtr'`, not *after*.\n",
    "\n",
    "Evaluation of `sumtr(3)`:\n",
    "\n",
    "```haskell\n",
    "sumtr 3\n",
    "==> sumtr' 0 3\n",
    "==> sumtr' (0 + 3) 2\n",
    "```\n",
    "\n",
    "The second argument `n` (here: `2`) has to be evaluated for pattern matching (to check if `n` is 0). The first argument `m` is kept as a *thunk*.\n",
    "\n",
    "```haskell\n",
    "==> sumtr' ((0 + 3) + 2) 1\n",
    "==> sumtr' (((0 + 3) + 2) + 1) 0\n",
    "==> (((0 + 3) + 2) + 1)\n",
    "```\n",
    "\n",
    "- Again, no stack frame *inside* sumtr' is kept.\n",
    "- This time, this is due to tail recursion, not laziness.\n",
    "- **But** we still build up *thunks* ...\n",
    "- ... and evaluation of the nested `(+)` still requires stack space, ...\n",
    "- ... but we do not actually need the laziness in this case.\n",
    "\n",
    "## Forcing strict evaluation\n",
    "\n",
    "**`($!) `** <br /> forces strict evaluation of the following operation\n",
    "\n",
    "```haskell\n",
    "($!) :: (a -> b) -> a -> b |infixr 0|\n",
    "```"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sumtrs :: Int -> Int\n",
    "sumtrs n = sumtrs' 0 n\n",
    "    where sumtrs' m 0 = m\n",
    "          sumtrs' m n = (sumtrs' $! (m+n)) (n-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Evaluation of `sumtrs 3`:\n",
    "\n",
    "```haskell\n",
    "sumtrs 3\n",
    "==> sumtrs' 0 3\n",
    "==> sumtrs' 3 2\n",
    "==> sumtrs' 5 1\n",
    "==> sumtrs' 6 0\n",
    "==> 6\n",
    "```\n",
    "\n",
    "The sum `(m + n)` is evaluated directly! No stack space is required, and `sumtrs` performs much better than the other versions.\n",
    "\n",
    "**Downside**: Consider a version of `sumtrs` with `(:)` instead of `(+)` and `[]` instead of `0`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 18,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "listUpToStrict :: Int -> [Int]\n",
    "listUpToStrict n = listUpToStrict' [] n\n",
    "    where listUpToStrict' m 0 = m\n",
    "          listUpToStrict' m n = (listUpToStrict' $! (n : m)) (n-1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "(*Side note: why the name?*) Then\n",
    "\n",
    "```haskell\n",
    "head (listUpToStrict 100000)\n",
    "```\n",
    "\n",
    "constructs the entire list before returning!\n",
    "\n",
    "---\n",
    "\n",
    "Finally, consider abstracting the common pattern from the concrete operation:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 20,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "operationUpToN :: (Int -> a -> a) -> a -> Int -> a\n",
    "operationUpToN operation start n = f start n\n",
    "    where f m 0 = m\n",
    "          f m n = f (operation n m) (n-1)\n",
    "          \n",
    "sumtr2 n = operationUpToN (+) 0 n\n",
    "listUpTo2 n = operationUpToN (:) [] n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Here, we do not know if it is better to force strictness, since the `operation` is not known in advance!"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## seq\n",
    "\n",
    "A more general form strict evaluation is `seq`:\n",
    "\n",
    "```haskell\n",
    "seq :: a -> b -> b\n",
    "```\n",
    "\n",
    "It evaluates the first argument strictly and returns the second argument. `($!)` is equivalent to\n",
    "\n",
    "```haskell\n",
    "f $! x = x `seq` f x\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## BangPatterns\n",
    "\n",
    "Special syntax for forcing string evaluation: function arguments marked with `!` are evaluated strictly:\n",
    "\n",
    "```haskell\n",
    "f !x = ...\n",
    "```\n",
    "\n",
    "Also available for `let`\n",
    "\n",
    "```haskell\n",
    "let !x = ...\n",
    "```\n",
    "\n",
    "and `where`\n",
    "\n",
    "```haskell\n",
    "where !x =\n",
    "```\n",
    "\n",
    "**Note:** *BangPatterns* need to be activated by writing\n",
    "\n",
    "```haskell\n",
    "{-# LANGUAGE BangPatterns #-}\n",
    "```\n",
    "\n",
    "at the top of the source file. In the notebook interface,"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    ":ext BangPatterns"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "should be used."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Summary: recursion and laziness\n",
    "    \n",
    "- General recursion usually performs badly. *Tail recursion*, *laziness* and *strictness analysis* can help if used properly.\n",
    "\n",
    "- *Important*: know the overall situation to write performant code.\n",
    "\n",
    "- *Tail recursion* is useful, but *thunks* should be avoided if no laziness is needed and performance or memory consumption is important.\n",
    "\n",
    "- *Rule of thumb*: Use laziness for *constructing* data and strictness for (numerical) *computation* or when the value has to be evaluated anyway (e.g. due to strictness of `(+)` above).\n",
    "\n",
    "- Haskell's evaluation model is quite complex, and the Haskell compiler often performs a lot of optimizations. Therefore:\n",
    "\n",
    "    - Effects of laziness, tail recursion, etc on running time are sometimes hard to predict.\n",
    "    \n",
    "    - In many cases, it is not necessary to force strictness unless you are sure that operations have to be performed anyway.\n",
    "    \n",
    "    - Often, it's better to test several options in the concrete case.\n",
    "    \n",
    "    - Prefer **writing generic code**, avoid **premature optimization**.\n",
    "    \n",
    "    - Bottlenecks can often be identified later ($\\Rightarrow$ profiling)\n",
    "\n",
    "Further information and tips: [Performance](http://www.haskell.org/haskellwiki/Performance) section on the HaskellWiki."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Haskell",
   "language": "haskell",
   "name": "haskell"
  },
  "language_info": {
   "codemirror_mode": "ihaskell",
   "file_extension": ".hs",
   "name": "haskell",
   "version": "7.10.2"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 0
}
