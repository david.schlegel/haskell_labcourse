# Haskell lecture 

A lecture to teach Haskell in a 2 weeks block lab course.

## viewing of the notebooks

you can view the notebooks with an actual ihaskell kernel for jupyter (ipython 3.x). 
It is also possible to view them with the nbviewer from ipython:

http://nbviewer.ipython.org/urls/projects.gwdg.de/projects/haskell/repository/revisions/master/raw/<nb>

for example: 

http://nbviewer.ipython.org/urls/projects.gwdg.de/projects/haskell/repository/revisions/master/raw/lecture/03%20lists%20and%20tuples.ipynb

# Installation and use

## Requirements

at minimum

 - ghc 7.10+
 - charts
 - ihaskell
 - hmatrix

## How to use IHaskell at home via SSH tunnel

### At the NAM

Create a file ~/.ipython/profile_default/ipython_notebook_config.py containing

    c = get_config()
    c.NotebookApp.port = <port>

where `<port>` should be replaced by a randomly chosen port number between 50000 and 60000. As it is possible that multiple users want to run notebook servers on a single computer, it is important to choose a unique port in order to avoid conflicts.

### On your home computer

Open a terminal and run

    ssh -L <port>:localhost:<port> <user>@<host>.num.math.uni-goettingen.de ihaskell-notebook

where `<port>` is the same port as chosen above, `<user>` is your user name and `<host>` is one of `c3`, `c4`, etc, up to `c7`. On Windows, use an ssh client like PuTTY and connect to one of the hosts with enabled forwarding of `<port>`.

You should now be able to direct your browser to `localhost:<port>` to access the notebook interface.

## How to use IHaskell at home via X2go

get and install x2goclient from http://wiki.x2go.org. 

Create a session entry with 

    - host: login.num.math.uni-goettingen.de or one of the compute-servers c3-c7.num.math.uni-goettingen.de 
    - session type: XFCE
    - login: your student username

then you can connect with your usual credentials and get a remote desktop (like sitting directly in front of the computers)

# Converting notebooks 
## converting md-files to ipython (and back)

you can use the `md2ipynb` script or notedown (https://github.com/aaren/notedown) .

## converting lhs to python (and back)
 
    ihaskell convert -i newton.lhs -o newton.ipynb 

# visualization with python

because haskell misses a good 3D visualization library we use python for it. There is a script which takes X, Y and Z matrices to make a surface or contour plot.

```
./vis.py 
usage: vis.py [-h] --xmat XMAT --ymat YMAT --zmat ZMAT [--triangles TRIANGLES]
              [--type {surf,wireframe,contour,surf_contour,mayavi_surf,triangles}]
```

